#include <iostream>
using namespace std;

class Ordenamiento{
	private:
		clock_t tiempo;
		float duracion;
	public:
	
		Ordenamiento(){}
		
		void mostrar_arreglo(int *&arreglo, int N){
			for(int i = 00; i<N; i++){
				cout<<arreglo[i]<< " ";
			}
		}
		
		void burbuja_menor(int *arreglo, int N){
			tiempo = clock();
			int aux;
			
			for(int i=1; i<N;i++){
				for(int j=N-1; j>=i; j--){
					if(arreglo[j-1]>arreglo[j]){
						aux = arreglo[j-1];
						arreglo[j-1] = arreglo[j];
						arreglo[j] = aux;
					}
				}
			}
			tiempo = clock() - tiempo;
			duracion = float(tiempo)*1000/CLOCKS_PER_SEC;
			cout<<"duracion: "<<duracion<<endl;
		}
		
		void burbuja_mayor(int *arreglo, int N){
			int aux;
			for(int i=N-1; i>1;i--){
				for(int j=0; j<i; j++){
					if(arreglo[j]>arreglo[j+1]){
						aux = arreglo[j];
						arreglo[j] = arreglo[j+1];
						arreglo[j+1] = aux;
					}
				}
			}
		}
	
};


int main(int argc, char *argv[]){
	int *arreglo;
	int *arreglo_aux;
	int N = stoi(argv[1]);
	arreglo = new int[N];
	
	for(int i=0;i<N;i++){
		arreglo[i] = (rand()%1000)+1;
		
	}
	
	Ordenamiento *metodos = new Ordenamiento();
	metodos-> mostrar_arreglo(arreglo, N);
	cout<<"\n"<<endl;
	arreglo_aux = arreglo;
	metodos->burbuja_menor(arreglo_aux, N);
	metodos-> mostrar_arreglo(arreglo_aux, N);
	cout<<"\n"<<endl;
	arreglo_aux = arreglo;
	metodos->burbuja_mayor(arreglo_aux, N);
	metodos-> mostrar_arreglo(arreglo_aux, N);
	
	
	return 0;
	
	
}
