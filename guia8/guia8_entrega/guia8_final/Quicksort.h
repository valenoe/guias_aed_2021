#ifndef QUICKSORT_H
#define QUICKSORT_H

#include <iostream>
using namespace std;

class Quicksort{
	private:
		// arreglo
		int *a;
		// elementos en su inerior
		int n; 
	
	public:
		Quicksort(int *a, int n);
		void ordena_quicksort();
		
};
#endif
