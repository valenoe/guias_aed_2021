#include <iostream>
#include <chrono>
using namespace std;

#include "Quicksort.h"

Quicksort::Quicksort(int *a, int n){
	this->a = a;
	this->n = n;
	
}

void Quicksort::ordena_quicksort(){
	/**
	 *  El metodo quicksort consiste en tomar un elemento de una posición 
	 * cualquiera, generalmente el primero, y ubicarlo en la posición 
	 * correcta, de tal forma que los elementos a la izquierda sean menores 
	 * o iguales a él y los de la derecha sean mayores o iguales a él. 
	 * 
	 * Estos pasos se repiten para los conjuntos de datos que quedan a 
	 * la izquierda y a la derecha del elemento. 
	 * 
	 * El proceso termina cuando todos esten en la posición correcta 
	 * (Alejandro Valdés, Métodos de Ordenamiento).
	 * */
	int tope, ini, fin, pos;
	int pilamenor[100];
	int pilamayor[100];
	int izq, der, aux;
	bool band;
  
	tope = 0;
	pilamenor[tope] = 0;
	pilamayor[tope] = n-1;
  
	while (tope >= 0) {
		ini = pilamenor[tope];
		fin = pilamayor[tope];
		tope = tope - 1;
    
		// reduce
		izq = ini;
		der = fin;
		pos = ini;
		band = true;
    
		 while (band) {
			while ((a[pos] <= a[der]) && (pos != der))
				der = der - 1;
        
			if (pos == der) {
				band = false;
			} else {
				aux = a[pos];
				a[pos] = a[der];
				a[der] = aux;
				pos = der;
				
				while ((a[pos] >= a[izq]) && (pos != izq))
					izq = izq + 1;
          
				if (pos == izq) {
					band = false;
				} else {
					aux = a[pos];
					a[pos] = a[izq];
					a[izq] = aux;
					pos = izq;
				}
			}
		}
    
		if (ini < (pos - 1)) {
			tope = tope + 1;
			pilamenor[tope] = ini;
			pilamayor[tope] = pos - 1;
		}
    
		if (fin > (pos + 1)) {
			tope = tope + 1;
			pilamenor[tope] = pos + 1;
			pilamayor[tope] = fin;
		}
	}
}

