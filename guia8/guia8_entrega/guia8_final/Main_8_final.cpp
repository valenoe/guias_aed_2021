#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include <chrono>
using namespace std;

#include "Quicksort.h"
#include "Seleccion.h"


void imprimir_vector(int *a, int n) {
	int i;

	for (i=0; i<n; i++) {
		cout<<"a["<<i<<"]="<<a[i]<<"  ";
	}
	cout<<"\n\n";
}

/* principal */
int main(int argc, char **argv) {
	
	if(argc!=3){
		cout << "Comando mal ejecutado, el programa no se podra inciar" << endl;
		cout << "Recuerde que el programa recibe 3 parametros de entrada, de la siguiente forma : " << endl;
		cout << "	./orden N VER"<<endl;
		cout <<"orden: nombre del programa \nN: Cantidad de elementos para el arreglo \nVER: S o N, ver el arreglo"<<endl;
		cout << "\n";
		exit(-1);
	}
	else{
	
		int N = stoi(argv[1]);
		
		
		
	
	// se evalúa si el número ingresado sea mayor que dos
		if(N <= 1){
			cout<<"Debe ingresar un numero mayor que 1 para que se pueda efectuar el orden"<<endl;
			exit(-1);
			
		} else{
			
	
	
			int *arreglo, *arreglo_S, *arreglo_Q;
			// Arreglo principal
			arreglo = new int[N];
			// Arreglo para Seleccion
			arreglo_S = new int[N];
			//Arreglo para Quicksort
			arreglo_Q = new int[N];
	
			
			/**Se convierte la primera letra del parametro argv[2]
			 * en una eltra mayúscula y se guarda en VER*/
			char VER = toupper(argv[2][0]);
			
			
    
			// llena el vector.
			int i;
			int elemento;
			srand ((unsigned) time(0));
			for (i=0; i<N; i++) {
				elemento = (rand() % 5000) + 1;
				arreglo[i] = elemento;
				arreglo_S[i] = elemento;
				arreglo_Q[i] = elemento;
		
			}
	
			/**Aquí se evalúa VER
			 * - Se muestra su opcion y la cantidad de elemtos que contiene
			 * el  arreglo
			 * - si N <= 200 se imprimen los elementos
			 * - ver_arreglo es un booleano que solose vulave verdadero 
			 * cuando es posible ver todos los elementos, este se ocupará 
			 * cuando haya que imprimir los elementos ordenados*/
			cout<<"Usted escogió "<<VER<<" ver el arreglo"<<endl;
			cout<<"El arreglo contiene "<<N<<" elementos"<<endl;
			bool ver_arreglo = false;
			if(VER== 'S'){
				
				if(N>200){
					cout<<"Su arreglo es muy grande como para mostrarlo"<<endl;
				} else {
					ver_arreglo = true;
					
					imprimir_vector(arreglo, N);
				}
			}
	
			Quicksort *q = new Quicksort(arreglo_S, N);
	
			Seleccion *s = new Seleccion(arreglo_Q, N);
	
			
			// Seleccion
			auto start_sel = std::chrono::steady_clock::now();
			s->ordena_seleccion();		
			auto end_sel = std::chrono::steady_clock::now();
	
			std::chrono::duration<double> duration_sel = end_sel - start_sel;
	
	
			// Quicksort
			auto start_qui = std::chrono::steady_clock::now();
			q->ordena_quicksort();
			auto end_qui = std::chrono::steady_clock::now();
	
			std::chrono::duration<double> duration_qui = end_qui - start_qui;
	
			cout<<"-------------------------------------------------------------"<<endl;
			cout<<"Método\t\t| Tiempo"<<std::endl;
			cout<<"-------------------------------------------------------------"<<endl;
	
			cout<<"Seleccion\t| " ;
			std::cout<< duration_sel.count()*1000<<" milisegundos"<<std::endl;
			cout<<"Quicksort\t| ";
			std::cout<< duration_qui.count()*1000<<" milisegundos"<<std::endl;
	
	
			cout<<"-------------------------------------------------------------"<<endl;
	
			
			if(ver_arreglo){
				cout<<"Seleccion\t| ";
				imprimir_vector(arreglo_S, N);
				cout<<"Quicksort\t| ";
				imprimir_vector(arreglo_Q, N);
				
			}
		}	
	}
	return 0;

}
  
  
  
