#include <iostream>
#include <chrono>
using namespace std;

#include "Seleccion.h"

Seleccion::Seleccion(int *a, int n){
	this->a = a;
	this->n = n;
	
	
}


void Seleccion::ordena_seleccion(){
	/**Busca el menor elemneto del arreglo para colocarlo en la primera
	 * posición,luego busca el segundo más pequeño para colocarlo en la 
	 * segunda posición, así sucesivamente.
	 * (Alejandro Valdés, Métodos de Ordenamiento)*/
	int i, menor, k, j;
  
	for (i=0; i<=n-2; i++) {
		/**lee la lista en orden y el elemnto menor será a[i] para compararlo
		 * en el siguiente ciclo*/
		menor = a[i];
		k = i;
    
		for (j=i+1; j<=n-1; j++) {
			/**Este cilo lee desde la posición i+1 en adelante
			 * EL menor de los elementos que esté por delnate de a[i] 
			 * concerva el titulo de menor y k guarda su posición en en arreglo*/
			if (a[j] < menor) {
				menor = a[j];
				k = j;
			}
		}
		/**Al finalizar cada ciclo, el n° menor, definido en el ciclo anterior,
		 * toma la posición a[i] y el a[i] original se va a la posición del menor 
		 * (a[k])*/
		a[k] = a[i];
		a[i] = menor;
	}
}



 
