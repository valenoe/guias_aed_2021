#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
//#include <iomanip>
#include <chrono>
using namespace std;

#include "MetodosOrden.h"




/* principal */
int main(int argc, char **argv) {
	int N = stoi(argv[1]);
	int *arreglo, *arreglo_S, *arreglo_Q;
	arreglo = new int[N];
	arreglo_S = new int[N];
	arreglo_Q = new int[N];
	
	
	string VER = argv[2];
	cout<<"VER "<<VER<<"  argv[2] "<<argv[2]<<endl; 


	
	 clock_t t1, t2;
  double secs;
    
	
	
    
	// llena el vector.
	int i;
	int elemento;
	srand ((unsigned) time(0));
	for (i=0; i<N; i++) {
		elemento = (rand() % 5000) + 1;
		arreglo[i] = elemento;
		arreglo_S[i] = elemento;
		arreglo_S[i] = elemento;
		
	}
	
	arreglo_S = arreglo;
	arreglo_Q = arreglo;
	
	MetodosOrden *metodos = new MetodosOrden();
	
	
	
	//metodos->mostrar_vector(N, arreglo);
	// Seleccion
	auto start_sel = std::chrono::steady_clock::now();
	metodos->Seleccion(N, arreglo_S);
	//ts2 = clock();
	auto end_sel = std::chrono::steady_clock::now();
	
	std::chrono::duration<double> duration_sel = end_sel - start_sel;
	
	
	//Quicksort
	auto start_qui = std::chrono::steady_clock::now();
//t1 = clock();
	metodos->Quicksort(N, arreglo);
	// t1 = clock();
	auto end_qui = std::chrono::steady_clock::now();

	std::chrono::duration<double> duration_qui = end_qui - start_qui;
	
	cout<<"-------------------------------------------------------------"<<endl;
	cout<<"Método\t\t| Tiempo"<<std::endl;
	cout<<"-------------------------------------------------------------"<<endl;
	
	cout<<"Seleccion\t| " ;
	std::cout<< duration_sel.count()*1000<<" milisegundos"<<std::endl;
	cout<<"Quicksort\t| ";
	std::cout<< duration_qui.count()*1000<<" milisegundos"<<std::endl;
	// secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
 
	
	cout<<"-------------------------------------------------------------"<<endl;
	
	cout<<VER<<endl;
	if(VER == "S" || VER == "s"){
		cout<<"hols"<<endl;
		if(N <= 2000){
			cout<<"Seleccion\t| ";
			metodos->mostrar_vector(N, arreglo_S);
			cout<<"Quicksort\t| ";
			metodos->mostrar_vector(N, arreglo_Q);
		} else {
			cout<<"su arreglo es muy grande para mostrarlo"<<endl;
		}
			
	}
	return 0;
}
