#ifndef METODOSORDEN_H
#define METODOSORDEN_H

#include <iostream>
using namespace std;

class MetodosOrden {
	
	private:
		clock_t tiempo, t2;
		double duracion;
		
	public:
		MetodosOrden();
		void Seleccion(int N, int *arreglo);
		void Quicksort(int N, int *arreglo);
		void mostrar_vector(int N, int *arreglo);
		
	
};
#endif 
