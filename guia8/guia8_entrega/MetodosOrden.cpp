#include <iostream>

using namespace std;

#include "MetodosOrden.h"


MetodosOrden::MetodosOrden(){}


void MetodosOrden::Seleccion(int N, int *arreglo){
	//tiempo = clock();
	int i, menor, k, j;
  
	for (i=0; i<=N-2; i++) {
		menor = arreglo[i];
		k = i;
    
		for (j=i+1; j<=N-1; j++) {
			if (arreglo[j] < menor) {
				menor = arreglo[j];
				k = j;
			}
		}
    
		arreglo[k] = arreglo[i];
		arreglo[i] = menor;
	}
/*	tiempo = clock() - tiempo;
	duracion = float(tiempo)*1000/CLOCKS_PER_SEC;
	
	cout<<"Seleccion\t| "<<duracion<<" milisegundos"<<endl;
	* */
}

void MetodosOrden::Quicksort(int N, int *arreglo){
	tiempo = clock();
	int tope, ini, fin, pos;
	int pilamenor[100];
	int pilamayor[100];
	int izq, der, aux;
	bool band;
  
	tope = 0;
	pilamenor[tope] = 0;
	pilamayor[tope] = N-1;
  
	while (tope >= 0) {
		ini = pilamenor[tope];
		fin = pilamayor[tope];
		tope = tope - 1;
    
		// reduce
		izq = ini;
		der = fin;
		pos = ini;
		band = true;//1;
    
		while (band){// == 1) {
			while ((arreglo[pos] <= arreglo[der]) && (pos != der))
				der = der - 1;
        
			if (pos == der) {
				band = false;//0;
			} else {
				aux = arreglo[pos];
				arreglo[pos] = arreglo[der];
				arreglo[der] = aux;
				pos = der;
        
				while ((arreglo[pos] >= arreglo[izq]) && (pos != izq))
					izq = izq + 1;
          
				if (pos == izq) {
					band = false;//0;
				} else {
					aux = arreglo[pos];
					arreglo[pos] = arreglo[izq];
					arreglo[izq] = aux;
					pos = izq;
				}
			}
		}
    
		if (ini < (pos - 1)) {
			tope = tope + 1;
			pilamenor[tope] = ini;
			pilamayor[tope] = pos - 1;
		}
    
		if (fin > (pos + 1)) {
			tope = tope + 1;
			pilamenor[tope] = pos + 1;
			pilamayor[tope] = fin;
		}
  
	}
	tiempo = clock() - tiempo;
	duracion = double(tiempo)/CLOCKS_PER_SEC;
	
	cout<<"Quicksort\t| "<<duracion*1000.0<<" milisegundos"<<endl;
	
}
void MetodosOrden::mostrar_vector(int N, int *arreglo){
	int i;

	for (i=0; i<N; i++) {
		cout<<"a["<<i<<"]="<<arreglo[i]<<"  ";
	}
	cout<<"\n\n";
}
	
	
	
	
	
	
/*
  secs = (double)(t2 - t1) / CLOCKS_PER_SEC;
  double setot = secs;
  cout<<secs*1000.0<<" milisegundos\n";
  cout<< fixed<<setprecision(2)<<setot * 1000.0<<"milisegundos\n";
	
  return 0;
}
*/
