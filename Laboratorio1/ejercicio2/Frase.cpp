#include <iostream>

using namespace std;

#include "Frase.h"
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


// Constructor de la clase
Frase::Frase(int n, string *caracteres){
	this->n = n;
	this->caracteres = caracteres;
}

// Metodos set y get de la clase
void Frase::set_caracteres(string *caracteres, int n){
	this->caracteres[n] = caracteres[n];
	
}
void Frase::set_n(int n){
	this->n = n;
}

string Frase::get_caracteres(){
	return this->caracteres[n];
}

int Frase::get_n(){
	return this->n;
	}

// Método específicos del problema
void Frase::cuenta_mayusculas_minusculas(string *caracteres, int n){
	/**
	 * Esta funciòn se encarga de contar las letrasminúsculas y mayúsculas
	 * de cada una de las frases, para eso utiliza un 2 ciclos for.
	 * 
	 * El primero recorre el arreglo de frases, dentro de èl se inicalizan los contadores
	 * de cada tipo de letra, se calcula el largo de cada frase y se imorime la frase 
	 * en la que va a buscar.
	 * 
	 * El segundo for recorre la frase, dentro hay dos condiciones:
	 * 		La primera pregunta si la letra actual es minúscula, si lo es crece el contador.
	 * 		La segunda pregunta si la letra actual es mayúscula, si lo es crece el contador.
	 * 
	 * Antes de finalizar el primer for, un contador de las letras totales de cada tipo aumenta.
	 * También se imprime el total de letras minúsculas y mayúsculas por frase.
	 * 
	 * Al final de la función se imprime el total de letras minúsculas y mayúsculas de todo el arreglo.
	 * */
	int i, j, cont_total_min=0, cont_total_may=0;
	
	
	for(i=0; i<n; i++){
		int largo = caracteres[i].length();
		int contador_min = 0, contador_may = 0;
		cout<<caracteres[i]<<endl;
		for(j=0; j<largo;j++){
			
			if(islower(caracteres[i][j])){
				contador_min++;
			}else if(isupper(caracteres[i][j])){
				contador_may++;
			}
	}
	cout<<"Esta frase tiene: "<<endl;
	cout<<"\t"<<contador_min<<" letras minúsculas"<<endl;
	cout<<"\t"<<contador_may<<" letras mayúsculas"<<endl;
	cont_total_min += contador_min;
	cont_total_may += contador_may;
}
cout<<"En total hay "<<endl;
cout<<"\t"<<cont_total_min<<" letras minúsculas"<<endl;
cout<<"\t"<<cont_total_may<<" letras mayúsculas"<<endl;




}


