#ifndef FRASE_H
#define FRASE_H

#include <stdlib.h>
#include <iostream>
using namespace std;

class Frase{
	private:
		int n = 0;
		string *caracteres= NULL;
	public:
		// Constructor de la clase
		Frase(int n, string *caracteres);
		
		// Metodos set y get de la clase
		void set_caracteres(string *caracteres, int n);
		void set_n(int n);
		string get_caracteres();
		int get_n();
		
		// Método específico del problema
		void cuenta_mayusculas_minusculas(string *caracteres, int n);
		
		
		
	
};
#endif 
