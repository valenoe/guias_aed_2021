

#include <iostream>
using namespace std;


void imprime_vector(int *vec, int n){
	for(int i=0; i<n; i++){
		cout<<vec[i]<<" ";
	}
	cout<<endl;
}

void inicializa_vector(int *vec, int n){
	for(int i=0; i<n; i++){
		vec[i] = i+1;
		
	}
}

void suma_del_cuadrado(int *vec, int n){
	//list<int> cuadrado;
	int suma = 0, i, numero;
	//int longitud = sizeof vec / sizeof vec[0];
	
	for(i=0; i<n; i++){
		numero = vec[i]*vec[i];
		suma = suma+ numero;
		//cuadrado.push_back(numero);
	}
	cout<<"La suma del cuadrado de los numeros es: "<<suma<<endl;

}
  

int main(int argc, char **argv){
	int n = atoi(argv[1]);
	
	
	int vector1[n];
	
	inicializa_vector(vector1, n);
	
	imprime_vector(vector1, n);
	
	suma_del_cuadrado(vector1, n);
	
	return 0;
}
