#include <iostream>
using namespace std;

#include "Operaciones.h"
#include <stdlib.h>
#include <stdio.h>


int main(int argc, char **argv){
	int n = atoi(argv[1]);// Convierte el valor de argv[1] a entero
	int vector[n];// crea el vector
	
	
	Operaciones operaciones = Operaciones(n, vector);

//	operaciones.set_arreglo(vector, n);
	
	operaciones.inicializar_arreglo(vector, n);
	
	operaciones.suma_del_cuadrado(vector, n);
	

	return 0;
}
