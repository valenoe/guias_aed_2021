#include <iostream>

using namespace std;

#include "Operaciones.h"
#include <stdlib.h>

// Constructor de la clase

Operaciones::Operaciones(int n, int *arreglo){
	
	this->n = n;
	this->arreglo = arreglo;
	}

// Metodos set y get de la clase
int Operaciones::get_arreglo(){
	
	return this->arreglo[n]; 
}

int Operaciones::get_n(){
	return this->n;
}

void Operaciones::set_n(int n){
	this->n = n;
}
       
       
void Operaciones::set_arreglo(int *arreglo, int n){
	this->arreglo[n] = arreglo[n]; 

}

// Métodos específicos del problema

void Operaciones::inicializar_arreglo(int *arreglo, int n){
	/** esta funcion se encarga de incertar numeros enteros en el arreglo
	 * e imprimirlos
	 * */
	cout<<"Números del vector"<<endl;
	for(int i=0; i<n; i++){
		arreglo[i] = i+1;
		cout<<arreglo[i]<<" ";
		
	}
	cout<<endl;
}
        
void Operaciones::suma_del_cuadrado(int *arreglo, int n){
	/**Esta funcion calcula el cuadrado de cada uno de los numeros del 
	 * arreglo y los suma a una variable definida inicialmente en 0 (suma)
	 * finalmente muestra la suma*/
	int suma = 0, i, numero;
	
	
	for(i=0; i<n; i++){
		numero = arreglo[i]*arreglo[i];
		suma = suma+ numero;
		
	}
	cout<<"La suma del cuadrado de los numeros es: "<<suma<<endl;

}
        
       
		
