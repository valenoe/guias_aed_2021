#ifndef OPERACIONES_H
#define OPERACIONES_H

//#include <list>
#include <stdlib.h>




class Operaciones {
    private:

		int n = 0;
		int *arreglo = NULL;
    public:
        // constructor
        Operaciones(int n, int *arreglo);
        
        // métodos get and set 
        int get_arreglo();
		int get_n();
       
		void set_n(int n);
        void set_arreglo(int *arreglo, int n);
        void inicializar_arreglo(int *arreglo, int n);
        void suma_del_cuadrado(int *arreglo, int n);
        
        		
		
        
};
#endif 
