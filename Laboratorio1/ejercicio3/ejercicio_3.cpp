#include <iostream>
using namespace std;

#include "Cliente.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

Cliente ingresa_clientes(Cliente c){
	/**
	 * Esta función pide los datos de los clientes,
	 * nombre, telefono, saldo y si está mororso o no.
	 * 
	 * Al final devuelve el objeto cliente con tdos sus datos completos. */
	cout<<"Ingrese los datos del cliente";
	
	
	string nombre, telefono;
	
	cout<<"\nNombre: ";
	
	cout<<" - ";
	
	getline(cin, nombre);
	c.set_nombre(nombre);
	
	
	cout<<"\nNumero de teléfono: ";
	
	getline(cin, telefono);
	c.set_telefono(telefono);
	
	
	int saldo;
	cout<<"\nSaldo: ";
	cin>>saldo;
	c.set_saldo(saldo);
	
	int moroso;
	cout<<"si no está moroso aprete 0,\nsi lo está, aprete cualquier otra tecla "<<endl;
	
	cin>>moroso;
	cin.ignore();
	c.set_moroso(moroso);
	
	
	
	return c;

}


void agrega_clientes(Cliente cliente[], int n){
	/**Se cera un ciclo con i<n para ingresar todos los 
	 * clinetes que la empresa espera.
	 * 
	 * Al interior del for se crean los clientes y depués se llama
	 * a una función que recolecta los datos y devuelve un cliente 
	 * completo. */
	int i;
	for (i=0; i<n; i++){
		cliente[i] = Cliente();
		cliente[i] =ingresa_clientes(cliente[i]);
		
		}
	
	
	}



void muestra_clientes(Cliente cliente[], int n){
	/**Se crea un ciclo para que recorra cada cliente e imprima
	 * sus datos
	 * */
	int i;
	for (i=0; i<n; i++){
		cout<<"\nCliente numero "<<i+1<<endl;
		
		cliente[i].imprimir_datos();
		}
	

}


int main(int argc, char **argv){
	int n = atoi(argv[1]);// Convierte el valor de argv[1] a entero

	
	Cliente clientes[n];
	cout<<"A continuación se le pedirá ingresar los datos de cada cliente"<<endl;
	agrega_clientes(clientes, n);
	
	
	muestra_clientes(clientes, n);
	
	
	
	return 0;
}
