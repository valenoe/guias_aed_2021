#include <iostream>

using namespace std;

#include "Cliente.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Constructor de la clase
Cliente::Cliente(){
	
}

// Metodos set y get de la clase
void Cliente::set_nombre(string nombre){
	this->nombre = nombre;
}
void Cliente::set_telefono(string telefono){
	this->telefono = telefono;
}
void Cliente::set_saldo(int saldo){
	this->saldo = saldo;
}
void Cliente::set_moroso(bool moroso){
	this->moroso = moroso;
}
		
string Cliente::get_nombre(){
	return this->nombre;
}
string Cliente::get_telefono(){
	return this->telefono;
}
int Cliente::get_saldo(){
	return this->saldo;
}
bool Cliente::get_moroso(){
	return this->moroso;
}
		
// Método específico del problema
void Cliente::imprimir_datos(){
	
	cout<<"\tNombre: "<<nombre<<endl;
	cout<<"\tNumero de telefono: "<<telefono<<endl;
	cout<<"\tSaldo: "<<saldo<<endl;
	if(moroso){
		cout<<"\tEstado: moroso"<<endl;
	}
	else{
		cout<<"\tEstado: no moroso"<<endl;
	
	}	
}
