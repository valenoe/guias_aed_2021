

#include <iostream>
using namespace std;

#include "Hash.h"
#include "Encadenamiento.h"

void imprimir_vector(int *a, int n) {
	int i;
	cout<<"\nArreglo:"<<endl;

	for (i=0; i<n; i++) {
		cout<<"a["<<i<<"]="<<a[i]<<endl;
	}
	cout<<"\n\n";
}

void elegir_ingreso(char modo, Hash *h, Encadenamiento *e){
	/**esta función se encarga de indicar que método de inserción 
	 * debe usar el programa*/
	int num;
	cout<<"\nIngrese el número: ";
	cin>>num;
	
	
	if(modo == 'L'){
		h->insercion_lineal(num);
		
	}else if(modo == 'C'){
		h->insercion_cuadratica(num);
		
	}else if(modo == 'D'){
		h->insercion_doble_direccion_hash(num);
		
	}else if(modo == 'E'){
		e->insercion_encadenamiento(num);
	}
	
}

void elegir_busqueda(char modo, Hash *h, Encadenamiento *e, bool busqueda){
	/**esta función se encarga de indicar que método de búsqueda 
	 * debe usar el programa*/
	int num;
	cout<<"Ingrese el número: ";
	cin>>num;
	
	/**Además la función eliminar (de la clase Hash) ocupa los métodos de
	 * busqueda para obtener información sobre el número que quiere eliminar
	 * pero tambié, se basa en un booleano, ya que buscar y eliminar están unidas,
	 * si el bool busqueda == true, estonces solo se va a buscar, no eliminar,
	 * en cambio si es false, se va a buscar y eliminar*/
	int *eliminar = new int[2];
	if(modo == 'L'){
		h->busqueda_lineal(num, eliminar);
		
	}else if(modo == 'C'){
		h->busqueda_cuadratica(num, eliminar);
		
	}else if(modo == 'D'){
		h->busqueda_doble_direccion_hash(num, eliminar);
		
	}else if(modo == 'E'){
		e->busqueda_encadenamiento(num, eliminar);
	}
	
	if(!busqueda){
		
		h->eliminar(num, eliminar);
	}
}

int main(int argc, char **argv){
	if(argc!=2){
		cout << "Comando mal ejecutado, el programa no se podrá inciar" << endl;
		cout << "Recuerde que el programa recibe 2 parametros de entrada, de la siguiente forma : " << endl;
		cout << "	./hash {L|C|D|E} "<<endl;
		cout <<"hash: nombre del programa \n{L|C|D|E}: lestra del metodo de resolución de colisiones"<<endl;
		cout << "\n";
		exit(-1);
	}
	else{
		
		cout<<"\n _______________________Metodos de Búsqueda _______________________\n"<<endl;
		
		string letra = argv[1];
		char metodo_colision = toupper(letra[0]);
		cout<<"Usté escogio el modo: "<<metodo_colision<<endl;
		cout<<"L = Prueba Lineal"<<endl;
		cout<<"C = Prueba Cuadrática"<<endl;
		cout<<"D = Doble Dirección Hash"<<endl;
		cout<<"E = Encadenamiento"<<endl;
		int opcion;
		
		int *arreglo;
		int N = 20;
		arreglo = new int[N];
		Hash *h = new Hash(arreglo, N);
		
		int primo = h->primo_(N);
		cout<<"N es: "<<N<<", se va a aplicar módulo de: "<<primo<<endl;
		
		Encadenamiento *e = new Encadenamiento(arreglo, N);
		
		// este bool se actualiza cada vez que se selecciona la opción 2 o 4;
		bool busqueda;
		
		 
		
		do{
			
			cout<<"\n\n\tMENU\n"<<endl;
			cout<<"Insertar número\t\t[1]"<<endl;
			cout<<"Buscar un número\t[2]"<<endl;
			cout<<"mostrar arreglo\t\t[3]"<<endl;
			cout<<"Eliminar número\t\t[4]"<<endl;
			cout<<"Salir\t\t\t[0]"<<endl;
			cout<<"Cualquier otra opcion significa salir"<<endl;
			cout<<"Opción: ";
			cin>>opcion;
			
			if(opcion == 1){
				elegir_ingreso(metodo_colision, h, e);
				imprimir_vector(arreglo, N);
				
				
			}
			else if(opcion == 2){
				busqueda = true;
				elegir_busqueda(metodo_colision, h, e, busqueda);
				imprimir_vector(arreglo, N);
			}
			else if(opcion == 3){
				imprimir_vector(arreglo, N);
			}
			else if(opcion == 4){
				busqueda = false;
				elegir_busqueda(metodo_colision, h, e, busqueda);
				imprimir_vector(arreglo, N);
			}
			else{
				
				opcion = 0;
			}
		}while(opcion != 0);
		
		
		

	}
	
 return 0;
}
