#include <iostream>


using namespace std;

#include "Encadenamiento.h"

Encadenamiento::Encadenamiento(int *arreglo, int N){
	this->arreglo = arreglo;
	
	this->N = N;
	
	}

void Encadenamiento::insercion_encadenamiento(int clave){
	// Función Hash módulo
	int primo = primo_(N);
	int D = clave % primo;
	
	/**Se crea el nodo del nuevo numero a ingresar con crear nodo
	 * */
	Nodo *temp;
	temp = crearNodo(clave, D);
	cout<<"temp info"<<temp->info<<endl;
	cout<<"temp sig"<<temp->sig<<endl;
	if(arreglo[D] == 0){
		arreglo[D] = clave;
		cout<<"La posición "<<D<<" está vacia"<<endl;
		
		Celda *c = crearindicealista(clave, D);
		// si la posición está vacía debería agregarse a una celda además
		// de al arreglo original, mi fallo fue al organixar las celdas ya que no supe como unirlas
	} else if((arreglo[D] == clave)){
		cout<<"Este numero ya se encuentra"<<endl;
	}
	else{
		
		
		
		Nodo *Q;
		// La idea era que este nodo Q tomara el valor del primer nodo 
		// insertdado en ese indice, para eso tendría que buscarlo
		// y devolverlo
		
		
		Q= temp->sig;
		while((Q != NULL) && (Q->info != clave)){
			/*
			 * El ciclo se encarga de tomar el valo de Q->sig hasta enconrar
			 * un espacio vacío (el final), se detendrá cuando lo encuantre o cuando encuantre la 
			 * el numero ya en la lista*/
			Q = Q->sig;
		}
		
		if(Q == NULL){
			/*Si encuentra el espacio vació va a gergar el nodo*/
			cout<<"este nodo está vacio"<<endl;
			Q = crearNodo(clave, D);
		}
		else if(Q->info != clave){
			cout<<"El dato ya está "<<endl;
		}
	}
	
}

void Encadenamiento::busqueda_encadenamiento(int clave, int *eliminar){
	// Función Hash módulo
	int primo = primo_(N);
	int D = clave % primo;
	
	if((arreglo[D] != 0) && (arreglo[D] == clave)){
		cout<<"La información está en la posición "<<D<<endl;
	}
	else{
		
		Nodo *temp;
		temp = new Nodo;
		
		temp->info = arreglo[D];
		temp->sig = NULL;
		
		// temp = buscarClave(D);
		Nodo *Q = temp->sig;
		while((Q != NULL) && (Q->info != clave)){
			Q = Q->sig;
		}
		
		if(Q == NULL){
			
			cout<<"La información no se enecuentra en la lista"<<endl;
		}
		else{
			cout<<"La información está en la lista "<<endl;
		}
	}
	



}

Nodo *Encadenamiento::crearNodo(int clave, int D){
	Nodo *aux = new Nodo;
	aux->info = clave;
	aux->sig = NULL;
	aux->indice = D;
	return aux; 
}

Celda *Encadenamiento::crearindicealista(int num, int indice){
	Celda *celda  = new Celda();
	
	celda->numero = num;
	celda->lista = NULL;
	celda->indice = indice;
	
	
	
	return celda;
}

int Encadenamiento::primo_(int N){
	/**Calcula el numero primo más cercano, 
	 * mediante un for que empieza en 2 busca un divisor del numero original,
	 * si es que encuentra uno noprimo se vuelve verdadero, o sea el n° original no era primo.
	 * 
	 * si noprimo es verdadero se llama recursivamente a esta función con el n° ooriginal -1
	 * al final va a devolver el n° primo más cercano.*/
	bool noprimo = false;

	for(int i=2; i<N; i++){
		if(N % i == 0){
			noprimo = true;
		}
	}
	if(noprimo){
		N = primo_(N-1);
	}

	return N;
}

	


