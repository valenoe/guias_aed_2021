#ifndef HASH_H
#define HASH_H

#include <iostream>
using namespace std;



class Hash{
	private:
		int *arreglo;
		int N;
	public:
		
		Hash(int *arreglo, int N);
		int hash_mod(int clave);
		 // Inserción y reasignación
		void insercion_lineal(int clave);
		void insercion_cuadratica(int clave);
		void insercion_doble_direccion_hash(int clave);
		//búsqueda
		void busqueda_lineal(int clave, int *eliminar);
		void busqueda_cuadratica(int clave, int *eliminar);
		void busqueda_doble_direccion_hash(int clave, int *eliminar);
		
		int primo_(int N);
		void eliminar(int clave, int *datos);
		
	
};
#endif
