#include <iostream>


using namespace std;

#include "Hash.h"

Hash::Hash(int *arreglo, int N){
	// se guardan los datos que se van a usar constantemente
	this->arreglo = arreglo;
	this->N = N;

}


int Hash::hash_mod(int clave){
	// Se ocupó la función Hash por módulo o división
	// pero antes se calcula el primo más cercano de N
	int primo = primo_(N);
	int D = clave % primo;
	
	return D;
	
}
		
void Hash::insercion_lineal(int clave){
	
	/**Primero se calcula el indice (D) por Hash
	 * Luego se pregunta si está vacia esa posición (con 0),
	 * Si es que lo está, el numero se ingresa inmediatamente y la función termina, 
	 * si no, se reasigna por Lineal.
	 * 
	 * Lineal:
	 * 		DX es el nuevo indice, inicialmente se le suma 1, y si llega a ser igual a 
	 * 		N se vuelve 0.
	 * 		El ciclo while tiene como objetivo principal buscar un espacio vacío,
	 * 		pero también se detiene por:
	 * 			-	DX sobrepase el limite (tambiénn está ontrolado dentro del ciclo)
	 * 			-	arreglo en la posición DX sea el mismo numero (Si ya está no lo va a agregar de nuevo)
	 * 			-	o que DX se iagula a D (ya dio vuelta el arreglo sin resultados positivos)
	 * 		
	 * 		Dentro del cilo DX crece en 1 y se vuelve 0 cuando DX es igual a N.
	 * 		Cuando se detenga el ciclo while se va a preguntar por cual de las opciones se detuvo
	 * 		y va a imprimir un comentario con el resultado.
	 *   
	 * */
	int D = hash_mod(clave);
	cout<<"número: "<<clave<<" D: "<<D<<endl;
	int DX = 0;
	
	if(arreglo[D] == 0){
		//INSERCIÓN
		arreglo[D] = clave;
		cout<<"La posición "<<D<<" está vacia"<<endl;
	} else if((arreglo[D] == clave)){
		cout<<"Este numero ya se encuentra"<<endl;
	}else{
		//REASIGNACIÓN
		cout<<"Colisión en "<<D<<endl;
		DX = D+1;
		if(DX == N){
				
				DX = 0;
			}
		while((DX <= N-1) && (arreglo[DX] != 0 ) && (arreglo[DX] != clave) && (DX != D)){
			cout<<"Colisión en "<<DX<<endl;
			DX = DX + 1;
	
			if(DX == N){

				DX = 0;
			}
			
		
		
		}
	
		if(arreglo[DX] == 0){
	
			cout<<"La posición "<<DX<<" está vacia"<<endl;
			arreglo[DX] = clave;
		}
		else if(arreglo[DX] == clave){
			cout<<"La clave ya se encontraba en el arreglo en "<<DX<<endl;
		}
		else if(DX != D){
			cout<<"No hay espacio disponible"<<endl;
		}
	}
}
void Hash::insercion_cuadratica(int clave){
	
	/**Primero se calcula el indice (D) por Hash
	 * Luego se pregunta si está vacia esa posición (con 0),
	 * Si es que lo está, el numero se ingresa inmediatamente y la función termina, 
	 * si no, se reasigna por Cuadrática.
	 * 
	 * Cuadrática:
	 * 		DX es el nuevo indice.
	 * 		I es el número al cual se le va a calcular el cuadrado y se le va a sumar a DX.
	 * 		El ciclo while tiene como objetivo principal buscar un espacio vacío,
	 * 		pero también se detiene por que el arreglo en la posición DX sea el mismo numero (Si ya está no lo va a agregar de nuevo).
	 * 		Dentro del ciclo:
	 * 				- I crece en 1.
	 * 				- Se suma el cuadrado de I a D.
	 * 				- Se pregunta si DX sobrepasó el limite del arreglo para volver todo a 0.
	 * 				- I se vuelve -1 porque al inicio del ciclo se le suma 1. 
	 * 		
	 * 		Cuando se detenga el ciclo whilese va a preguntar por cual de estas opciones se detuvo
	 * 		y va a imprimir un comentario con el resultado.
	 *   
	 * */
	int D = hash_mod(clave);
	int DX, I;
	cout<<"clave: "<<clave<<" D: "<<D<<endl;
	
	
	if(arreglo[D] == 0){
		//INSERCIÓN
		arreglo[D] = clave;
		cout<<"La posición "<<D<<" está vacia"<<endl;
	} else if((arreglo[D] == clave)){
		cout<<"Este numero ya se encuentra"<<endl;
	} else{
		//REASIGNACIÓN
		cout<<"Colisión en "<<D<<endl;
		I = 1;
		DX = (D + (I*I));
		
		while((arreglo[DX] !=0) && (arreglo[DX] != clave)){
			cout<<"Colisión en "<<DX<<endl;
			I = I + 1;
			//cout<<"Ia la 2 "<<I*I<<" D: "<<D<<endl;
			DX = (D + (I*I));
			
			if(DX >  N-1){
				I = 0;
				DX = 0;
				D = 0;
				
			}
		
			
		}
		
		if(arreglo[DX] == 0){
	
			cout<<"La posición "<<DX<<" está vacia"<<endl;
			arreglo[DX] = clave;
		}
		else if(arreglo[DX] == clave){
			cout<<"La clave ya se encontraba en el arreglo"<<DX<<endl;
		}
		
	
	}
}
void Hash::insercion_doble_direccion_hash(int clave){
	/**Primero se calcula el indice (D) por Hash
	 * Luego se pregunta si está vacia esa posición (con 0),
	 * Si es que lo está, el numero se ingresa inmediatamente y la función termina, 
	 * si no, se reasigna por Doble dirección hash.
	 * 
	 * Doble dirección hash:
	 * 		DX es el nuevo indice al cual se le aplica la "nueva" función Hash (hash(D+1))
	 * 		El ciclo while tiene como objetivo principal buscar un espacio vacío,
	 * 		pero también se detiene por:
	 * 			-	DX sobrepase el limite (tambiénn está ontrolado dentro del ciclo)
	 * 			-	arreglo en la posición DX sea el mismo numero (Si ya está no lo va a agregar de nuevo)
	 * 			-	o que DX se iagula a D (ya dio vuelta el arreglo sin resultados positivos)
	 * 		
	 * 		Dentro del cilo se vuelve a plicar la "nueva" función Hash a DX y se vuelve 0 cuando DX es igual a N.
	 * 		Cuando se detenga el ciclo while se va a preguntar por cual de las opciones se detuvo
	 * 		y va a imprimir un comentario con el resultado.
	 *   
	 * */
	int D = hash_mod(clave);
	int DX;
	cout<<"clave: "<<clave<<" D: "<<D<<endl;
	if(arreglo[D] == 0){
		//INSERCIÓN
		arreglo[D] = clave;
		cout<<"La posición "<<D<<" está vacia"<<endl;
	} else if((arreglo[D] == clave)){
		cout<<"Este numero ya se encuentra"<<endl;
	}else{
		//REASIGNACIÓN
		cout<<"Colisión en "<<D<<endl;
		DX = hash_mod(D+1);
		if(DX == N){
				
				DX = 0;
			}
		while((DX <= N-1) && (arreglo[DX] != 0) && (arreglo[DX] != clave) && (DX != D)){
			cout<<"Colisión en "<<DX<<endl;
			
			DX = hash_mod(DX+1);
			if(DX == N){
				
				DX = 0;
			}
			
			
		}
		
		if(arreglo[DX] == 0){
	
			cout<<"La posición "<<DX<<" está vacia"<<endl;
			arreglo[DX] = clave;
		}
		else if(arreglo[DX] == clave){
			cout<<"La clave ya se encontraba en el arreglo en "<<DX<<endl;
		}
		else if(DX != D){
			cout<<"No hay espacio disponible"<<endl;
		}
	}
}

		
void Hash::busqueda_lineal(int clave, int *eliminar){
	/**Se calcula e imprime D (indice por Hash)
	 * primero se pregunta si está en arreglo en D y se imprime si es que está, 
	 * si no, se busca con otro indice (DX) por Lineal.
	 * 
	 * Lineal: consisten en aumentra 1 a DX y buscar por un ciclo while
	 * El ciclo se detendrá cuando:
	 * -	encuentre un espacio vacío, si se  ingresó el numero por iserción lineal, 
	 * 		no debería haber un espacio vacío antes de encontrarlo
	 * - 	Encontró el número
	 * -	o Recorrió todo el arreglo (DX == D)
	 * 
	 * Después de detenerse se pregunta por qué e imprime el resultado*/
	int D = hash_mod(clave);
	cout<<"clave: "<<clave<<" D: "<<D<<endl;
	int DX = 0;
	
	if((arreglo[D] != 0) && (arreglo[D] == clave)){
		cout<<"La información está en la posición "<<D<<endl;
		eliminar[0] = D;
		eliminar[1] = 1;
	}
	else{
		DX = D+1;
		if(DX == N){
				
				DX = 0;
			}
		
		while((DX <= N-1) && (arreglo[DX] != 0 ) && (arreglo[DX] != clave) && (DX != D)){
			DX = DX + 1;

			if(DX == N){
	
				DX = 0;
			}
	
		
		}
		if(arreglo[DX] == 0 || DX == D){
	
			cout<<"La información no se encuentra en el arrgelo"<<endl;
			eliminar[0] = D;
			eliminar[1] = 0;
			
		}
		else {
		
			cout<<"La información está en la posición "<<DX<<endl;
			eliminar[0] = DX;
			eliminar[1] = 1;
		}
	}
}
void Hash::busqueda_cuadratica(int clave, int *eliminar){
	/**Se calcula e imprime D (indice por Hash)
	 * primero se pregunta si está en arreglo en D y se imprime si es que está, 
	 * si no, se busca con otro indice (DX) por Cuadrática.
	 * 
	 * Cuadrática: consisten en tener una varialble (I) a la cual se le alcula 
	 * el cuadrado y se suma a D y buscar por un ciclo while (si Dx sobrepasa N+1 se vuelve 0)
	 * 
	 * El ciclo se detendrá cuando:
	 * -	encuentre un espacio vacío, si se  ingresó el numero por iserción Cuadrática, 
	 * 		no debería haber un espacio vacío antes de encontrarlo.
	 * - 	Encontró el número
	 * 
	 * 
	 * Después de detenerse se pregunta por qué e imprime el resultado*/
	int D = hash_mod(clave);
	int DX, I;
	if((arreglo[D] != 0) && (arreglo[D] == clave)){
		cout<<"La información está en la posición "<<D<<endl;
		eliminar[0] = D;
		eliminar[1] = 1;
	}
	else{
		I = 1;
		DX = (D + (I*I));
		
		while((arreglo[DX] !=0) && (arreglo[DX] != clave)){
			I = I + 1;
			DX = (D + (I*I));
			
			if(DX >  N-1){
				I = 0;
				DX = 0;
				D = 0;
				
			}
		//	cout<<"DX22 "<<DX<<endl; 
			
		}
		
		if(arreglo[DX] == 0){
			cout<<"La información no se enecuentra en el arrgelo"<<endl;
			eliminar[0] = D;
			eliminar[1] = 0;
		}
		else{
			cout<<"La información está en la posición "<<DX<<endl;
			eliminar[0] = DX;
			eliminar[1] = 1;
		}
	}
}
void Hash::busqueda_doble_direccion_hash(int clave, int *eliminar){
		/**Se calcula e imprime D (indice por Hash)
	 * primero se pregunta si está en arreglo en D y se imprime si es que está, 
	 * si no, se busca con otro indice (DX) por Doble dirección hash.
	 * 
	 * Doble dirección hash: consisten en calcular de nuevo el incie por la función Hash
	 * solo que ahora no se ingresa el numero inicial, sino que D+1; y buscar por un ciclo while
	 * El ciclo se detendrá cuando:
	 * -	encuentre un espacio vacío, si se  ingresó el numero por Doble dirección hash, 
	 * 		no debería haber un espacio vacío antes de encontrarlo.
	 * - 	Encontró el número
	 * -	o Recorrió todo el arreglo (DX == D)
	 * 
	 * Después de detenerse se pregunta por qué e imprime el resultado*/
	int D = hash_mod(clave);
	int DX;
	
	if((arreglo[D] != 0) && (arreglo[D] == clave)){
		cout<<"La información está en la posición "<<D<<endl;
		eliminar[0] = D;
		eliminar[1] = 1;
	}
	else{
		DX = hash_mod(D+1);
	
		
		while((DX <= N-1) && (arreglo[DX] != 0) && (arreglo[DX] != clave) && (DX != D)){
			DX = hash_mod(DX+1);
			
			
		}
		
		if((arreglo[DX] == 0) || (arreglo[DX] != clave)){
			cout<<"La información no se enecuentra en el arrgelo"<<endl;
			eliminar[0] = D;
			eliminar[1] = 0;
		}
		else{
			cout<<"La información está en la posición "<<DX<<endl;
			eliminar[0] = DX;
			eliminar[1] = 1;
		}
	}
	
}

int Hash::primo_(int N){
	/**Calcula el numero primo más cercano, 
	 * mediante un for que empieza en 2 busca un divisor del numero original,
	 * si es que encuentra uno noprimo se vuelve verdadero, o sea el n° original no era primo.
	 * 
	 * si noprimo es verdadero se llama recursivamente a esta función con el n° ooriginal -1
	 * al final va a devolver el n° primo más cercano.*/
	bool noprimo = false;

	for(int i=2; i<N; i++){
		if(N % i == 0){
			noprimo = true;
		}
	}
	if(noprimo){
		N = primo_(N-1);
	}

	return N;
}

void Hash::eliminar(int clave, int *datos){
	/**Eliminar recibe e. número a eliminar solo para mostralo y
	 * el arreglo datos que contiene:
	 * -	datos[0]: la posicoón del número en el arreglo original,
	 * 				esta proviene de los métodos de búsqueda
	 * -	datos[1]: un int que indica :
	 * 		-	1: sí está el número
	 * 		-	2: no está el número
	 * 
	 * solo si es que está el número (datos[1] == 1) se elimina o se vuelve 0. 
	 * 
	 * 
	 **/
	if(datos[1] == 1){
		arreglo[datos[0]] = 0;
		cout<<clave<<" ha sido eliminado"<<endl;
	}
}


