#ifndef ENCADENAMIENTO_H
#define ENCADENAMIENTO_H

#include <iostream>
using namespace std;

// Fallo en encadenamineto

typedef struct _Nodo {
	
	int info;
	int indice;
    struct _Nodo *sig;

} Nodo;

typedef struct _Celda{
	// La idea era cerar celdas que gardaran el numero
	//inicial que se inserta en el arreglo
	// pero no logré desarrollarlo
	// Nodo *lista levaria a una la lasta de numeros siguientes
	
	int numero;
	int indice;
	Nodo *lista;

} Celda;



class Encadenamiento{

	private: 
		int *arreglo;
		int N;
	public:
		Encadenamiento(int *arreglo, int N);
		void busqueda_encadenamiento(int clave, int *eliminar);
		void insercion_encadenamiento(int clave);
		Nodo *crearNodo(int clave, int indice);
		
		// esta función crearía las celdas
		Celda *crearindicealista(int num, int indice);
		int primo_(int N);
		
		
		// falta función buscarClave(int indice);
		// y agregar de forma ordenada celdass (por la clave)
};
#endif


