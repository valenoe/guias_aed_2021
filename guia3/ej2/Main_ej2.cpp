#include <iostream>
using namespace std;

#include "Lista.h"

void ingresar_datos(Lista *l){
	/**
	 * Esta función se encarga de ingresar números a las listas
	 * para eso pide lacantidad deseada de números y mediante un 
	 * ciclo for empieza a pedir números para llamar con ellos a la 
	 * función crear que lo va a ingrear de menos a mayor a la lista*/

	int tamanio, x =0;
	cout<<"\nIngrese la cantidad de elementos que va a querer en la lista"<<endl;
	cin>>tamanio;
	
	cout<<endl;
	for(int i =0; i<tamanio; i++){
		cout<<"Ingrese el nuevo numero: ";
		cin>>x;
		l->crear(x);
	}
	

}




int main(void){
	
	
	Lista *lista1 = new Lista();
	Lista *lista2 = new Lista();
	Lista *lista3 = new Lista();
 
	
	ingresar_datos(lista1);
	ingresar_datos(lista2);
	
	lista3->leer_otra_lista_y_unir(lista1);
	lista3->leer_otra_lista_y_unir(lista2);
	
	cout<<"\nLista 1"<<endl;
	lista1->imprimir();
	cout<<"\nLista 2"<<endl;
	lista2->imprimir();
	cout<<"\nLista 3"<<endl;
	lista3->imprimir();
	
	delete lista1;
	delete lista2;
	delete lista3;

	return 0;
}

