ejercicio 2 - guia 3

Este ejercicio pide crear 2 listas enlazadas ordenadas y crear una tercera que resulte de la mezcla de las dos.

Para ello el usuario ingresa los datos de ambas listas, primero se le va a pedir cuantos datos
quiere ingresar.

Ejemplo: 
		lista 1: "1 - 5 - 7"
		lista 2: "5 - 8 - 11"
		lista resultante: "1 - 5 - 5 - 7 - 8 - 11"
		
Después de llenar ambas listas se imprimirán las tres, todas están ordenadas.

Para ejecutar el programa debe compilar primero make y luego "./ejercicio2"
