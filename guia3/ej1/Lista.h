#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;


// define la estructura del nodo. 
typedef struct _Nodo {
	
	int numero;
    struct _Nodo *sig;

} Nodo;

class Lista {
    private:
        Nodo *primero = NULL;

    public:
        /* constructor*/
        Lista();
       
        void crear (int numero);
        /* imprime la lista. */
        void imprimir ();
        
      
};
#endif
