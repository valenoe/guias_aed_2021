ejercicio 1 - guia 3

Este ejercicio pide crear una lista de números enteros ordenados.

Para ello se crea un objeto lista y se le pide al usuario la cantidad de números que quiere 
ingresar, con un ciclo se le va pidiendo cada número.

Dentro del ciclo se llama a la función crear() y esta se encarga de insertar los números ordenados,
en su interior tiene unas condiciones, cada una con un resulado diferente, estas son para saber en 
que posisción colocarlos.

la primera condición es si la lista está vacía o si el nº a ingresar es menor que el primero, en 
este caso se agrega al principio el nuevo número.

para las siguientes se ocupa un ciclo que la recorre hasta que llegue al final o hasta que se 
encuentre con un nº mayor.

si llega al final, se agrega al final el nº, si no, o sea, si el ciclo encontró un elemento mayor 
al que se ingresará, se agrega en medio del anterior y el mayor.

Para ejecutar el programa debe compilar primero make y luego "./ejercicio1" 

