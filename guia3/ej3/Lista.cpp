#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){
}

void Lista::crear(int numero){
	
	Nodo *temp;
	
	
	temp = new Nodo;
	
	temp->numero = numero;
	temp->sig = NULL;
	
	
	/**
	 * Se van a evaluar dos cosas:
	 *  1) Que el primero elemento esté vacío.
	 *  2) Que el numero que va a entrar sea menor que el primero. 
	 * 
	 * en ambos casos el objeto siguiente toma el valor del primero
	 * y primero toma el valor de temp
	 * */
	if(this->primero == NULL || numero < this->primero->numero)
	{
		temp->sig = this->primero;
		this->primero = temp;
		
	}
	else{
		/**
		 * Creacion de otro nodo temporal que va a simular como el primer 
		 * objeto de la lista 
		 * */
		Nodo *otro_temp, *otro_temp_anteror;
		otro_temp = this->primero;
			
		while(otro_temp->sig != NULL && otro_temp->sig->numero < numero){
			/**
			 * Este while no va a funcionar cuando:
			 *  1) El nodo siguiente sea NULL, esto es para no seguir 
			 * 		avanzando si llega al final
			 *  2) El nodo siguente es mayor al número que se va a ingresar
			 *  
			 * si no se cumplen ninguna de estas condiciones, enonces 
			 * otro_temp va a tomar el valor siguiente
			 * */
			otro_temp = otro_temp->sig;
		}
		
		if(otro_temp->sig != NULL){
			/** si la condicion para que el while se rompiera es que 
			 * otro_temp->sig es mayor al numero que se quiere ingresar
			 * pero no estaba al finalde la lista, entonces temp->sig va a 
			 * apuntar a otro_temp->sig.
			 * 
			 * */
			temp->sig = otro_temp->sig;
		}
	
		
		/**
		 * Otro temp->sig siempre va a terminar apuntado a temp
		 * */
		otro_temp->sig = temp;
		
		
		
	}
	/** se llama a la función imprimir para que se muestre el estado de 
	   la lista cada vez que se agregue un nuevo elemento
	 */
	
}

void Lista::imprimir(){
	/**
	 * En esta funcoón se crea un nuevo nodo temporal que va a simular 
	 * el primer objeto de la lista.
	 * Este se va a usar para imprimir los objetos de la lista mediante 
	 * un ciclo while
	 * 
	 * while: 
	 * 		Se repetirá hasta que se encuetre con un espacio vacio.
	 * 		Imprimirá el valor de numero perteneciente a temp y luego 
	 * 		temp va a apuntar al espacio siguiente.
	 *  */
	
	
	Nodo *temp = this->primero;
	
	while(temp != NULL){
		cout<<temp->numero<<endl;
		temp = temp->sig;
	}
}


void Lista::busqueda_faltantes(){
	/**
	 * Se crean 2 nodos, q para recorrer la lista y r para ser el que viene
	 * exactamente detrás */
	Nodo *q, *r;
	q = this->primero;
	
	while(q->sig != NULL){
		r = q;
		q = q->sig;
		
		if((q->numero -1) > r->numero){
		/** q->numero -1 representa al antesesor del nº que representa q en ese momento
		 * si este nº es mayor que el de r significa que este último no es el antesor
		 * de q, entonces va a haber qu ellenar ese espacio.
		 * 
		 * entonces el for se va a encargar de crear numeros desde r+1 hasta q-1
		 * e integrarlos a la lista mediante la función crear()
		 * */
			for(int i = r->numero + 1; i< q->numero; i++){
				crear(i);
			}
		}
	}

}
