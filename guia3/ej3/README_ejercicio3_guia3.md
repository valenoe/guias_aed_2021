ejercicio 3 - guia 3

Este ejercicio solicitan números enteros para una lista enlazada simple ordenada, si hay valores
no correlativos el programa deberá llenar los espacios falatantes, por ejemplo:
		lista original "10 - 15 - 16 - 18"
		lista nueva "10 - 11 - 12 - 13 - 14 - 15 - 16 - 17 - 18" 
		
Este programa solo necesita compilar el archivo make y luego "./ejercicio3" para que inicie.

Ya abierto se le pedirá al usuario que indique la cantidad de elementos que ingresará inicialmente 
a la lista.

Luego se imprimirán esos elementos e inmediatamente después se imprimirá la lista con los espacios llenos.

Esto se logra debido a que tiene una función busqueda_faltantes() que se encarga de revisar si 
los datos originales no son correlativos, si es así, se crean los números faltantes y se ingresan 
a la lista mediante la funcón crear().
