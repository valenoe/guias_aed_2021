
#include <iostream>
using namespace std;

#include "Lista.h"

int main(void){
	
	// falta validacion de numeros enteros
	
	Lista *lista = new Lista();
	
	int tamanio, x =0;
	cout<<"Ingrese la cantidad de elementos que va a querer en la lista"<<endl;
	cin>>tamanio;
	cout<<endl;
	for(int i =0; i<tamanio; i++){
		cout<<"Ingrese el nuevo numero: ";
		cin>>x;
		lista->crear(x);
	}
	cout<<"\nEstado de la lista original"<<endl;
	lista->imprimir();
	lista->busqueda_faltantes();
	cout<<"\nEstado de la lista nueva"<<endl;
	lista->imprimir();

	delete lista;

	return 0;
}
