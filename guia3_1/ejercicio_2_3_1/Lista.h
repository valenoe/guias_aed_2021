#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;


// define la estructura del nodo. 
typedef struct _Nodo {
	
	string nombre;
    struct _Nodo *sig;

} Nodo;

class Lista {
    private:
        Nodo *primero = NULL;
        

    public:
        /* constructor*/
        Lista();
       
       
        void crear_ordenada(string nombre);
        
        /* imprime la lista. */
        void imprimir ();
        
      
};
#endif
