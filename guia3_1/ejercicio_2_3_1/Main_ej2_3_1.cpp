#include <iostream>
#include <ctype.h>

using namespace std;
#include "Lista.h"



int main(){
	
   
   
   Lista *lista = new Lista();
  
	
	int tamanio;
	string x;
	cout<<"Ingrese la cantidad de nombres que va a querer en la lista"<<endl;

	cin>>tamanio;
	
	for(int i =0; i<tamanio; i++){
		cin.ignore();
		cout<<"\nIngrese el nuevo nombre: ";
		getline(cin, x);
		lista->crear_ordenada(x);
		
		cout<<"\nPresiones enter para continuar"<<endl;
		
	}
	
	cout<<"\nLista: "<<endl;
	lista->imprimir();
	
	

	delete lista;

	return 0;
	
  
}
