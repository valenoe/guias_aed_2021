#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){
}


void Lista::crear_ordenada(string nombre){
	
	Nodo *temp;
	
	
	temp = new Nodo;
	
	temp->nombre = nombre;
	temp->sig = NULL;
	
	// letra repersenta la primera letra del nombre que va a ingresar en mayúscula
	char letra = toupper(nombre[0]);
	
	
	/**
	 * Se van a evaluar dos cosas:
	 *  1) Que el primero elemento esté vacío.
	 *  2) Que la primera letra del nombre que va a entrar vaya antes 
	 * 		que la del primer nombre. 
	 * 
	 * en ambos casos el objeto siguiente toma el valor del primero
	 * y primero toma el valor de temp
	 * */
	if(this->primero == NULL || letra < toupper(this->primero->nombre[0]))
	{
		temp->sig = this->primero;
		this->primero = temp;
		
		
	}
	else{
		/**
		 * Creacion de otro nodo temporal que va a simular como el primer 
		 * objeto de la lista 
		 * */
		Nodo *otro_temp;
		otro_temp = this->primero;
			
		while(otro_temp->sig != NULL && toupper(otro_temp->sig->nombre[0]) < letra){
			/**
			 * Este while no va a funcionar cuando:
			 *  1) El nodo siguiente sea NULL, esto es para no seguir 
			 * 		avanzando si llega al final
			 *  2) El nodo siguente va después que el nombre que se va a ingresar
			 *  
			 * si no se cumplen ninguna de estas condiciones, enonces 
			 * otro_temp va a tomar el valor siguiente
			 * */
			otro_temp = otro_temp->sig;
		}
		
		if(otro_temp->sig != NULL){
			/** si la condicion para que el while se rompiera es que 
			 * otro_temp->sig va después que el nombre que se quiere ingresar
			 * pero no estaba al final de la lista, entonces temp->sig va a 
			 * apuntar a otro_temp->sig.
			 * 
			 * */
			temp->sig = otro_temp->sig;
		
		}
	
		
		/**
		 * Otro temp->sig siempre va a terminar apuntado a temp
		 * */
		otro_temp->sig = temp;
		
		
		
	}
	cout<<"\nSe ha agregado unnuevo nombre: "<<endl;
	cout<<"Estado actual de la lista"<<endl;
	imprimir();
	
}

void Lista::imprimir(){
	/**
	 * En esta funcoón se crea un nuevo nodo temporal que va a simular 
	 * el primer objeto de la lista.
	 * Este se va a usar para imprimir los objetos de la lista mediante un ciclo while
	 * 
	 * while: 
	 * 		Se repetirá hasta que se encuetre con un espacio vacio.
	 * 		Imprimirá el valor del nombre perteneciente a temp y luego temp
	 * 		va a apuntar al espacio siguiente.
	 *  */
	
	Nodo *temp = this->primero;
	
	while(temp != NULL){
		cout<<" el nombre es:"<<temp->nombre<<endl;
		temp = temp->sig;
	}
}



