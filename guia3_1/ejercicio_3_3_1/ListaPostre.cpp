#include <ctype.h>
#include <string.h>
#include <iostream>
using namespace std;

#include "ListaPostre.h"
#include "Postre.h"

ListaPostre::ListaPostre(){
}


void ListaPostre::crear_ordenada(Postre *postre){
	
	Nodo2 *temp;
	
	
	temp = new Nodo2;
	
	temp->postre = postre;
	temp->sig = NULL;
	
	// letra repersenta la primera letra del nombre que va a ingresar en mayúscula
	char letra = toupper(postre->get_nombre()[0]);
	
	
	/**
	 * Se van a evaluar dos cosas:
	 *  1) Que el primero elemento esté vacío.
	 *  2) Que la primera letra del nombre que va a entrar vaya antes 
	 * 		que la del primer nombre. 
	 * 
	 * en ambos casos el objeto siguiente toma el valor del primero
	 * y primero toma el valor de temp
	 * */
	if(this->primero == NULL || letra < toupper(this->primero->postre->get_nombre()[0]))
	{
		temp->sig = this->primero;
		this->primero = temp;
		
		
	}
	else{
		/**
		 * Creacion de otro nodo temporal que va a simular como el primer 
		 * objeto de la lista 
		 * */
		Nodo2 *otro_temp;
		otro_temp = this->primero;
			
		while(otro_temp->sig != NULL && toupper(otro_temp->sig->postre->get_nombre()[0]) < letra){
			/**
			 * Este while no va a funcionar cuando:
			 *  1) El nodo siguiente sea NULL, esto es para no seguir 
			 * 		avanzando si llega al final
			 *  2) El nodo siguente va después que el nombre que se va a ingresar
			 *  
			 * si no se cumplen ninguna de estas condiciones, enonces 
			 * otro_temp va a tomar el valor siguiente
			 * */
			otro_temp = otro_temp->sig;
		}
		
		if(otro_temp->sig != NULL){
			/** si la condicion para que el while se rompiera es que 
			 * otro_temp->sig va después que el nombre que se quiere ingresar
			 * pero no estaba al final de la lista, entonces temp->sig va a 
			 * apuntar a otro_temp->sig.
			 * 
			 * */
			temp->sig = otro_temp->sig;
		
		}
	
		
		/**
		 * Otro temp->sig siempre va a terminar apuntado a temp
		 * */
		otro_temp->sig = temp;
		
		
		
	}
	cout<<"\nSe ha agregado unnuevo nombre: "<<endl;
	cout<<"Estado actual de la lista"<<endl;
	imprimir();
	
}

void ListaPostre::imprimir(){
	/**
	 * En esta funcoón se crea un nuevo nodo temporal que va a simular 
	 * el primer objeto de la lista.
	 * Este se va a usar para imprimir los objetos de la lista mediante un ciclo while
	 * 
	 * while: 
	 * 		Se repetirá hasta que se encuetre con un espacio vacio.
	 * 		Imprimirá el valor del nombre perteneciente a temp y luego temp
	 * 		va a apuntar al espacio siguiente.
	 *  */
	
	Nodo2 *temp = this->primero;
	
	if(temp == NULL){
		cout<<"La lista está vacía"<<endl;
	}
	
	while(temp != NULL){
		cout<<" el nombre es:"<<temp->postre->get_nombre()<<endl;
		temp = temp->sig;
	}
	
	
}

void ListaPostre::eliminar(string nombre){
	/**para eliminar se necesitan 2 nodos auxiliares, 
	 * uno que va a recorre la lista y otro que lo reemplazará en caso
	 * de encontrar el objeto y que este no sea el primero*/
	Nodo2 *q, *t;
	int band = 1;
	q = this->primero;
	string nombre_postre = Mayuscula(nombre);
	
	if(q == NULL){
		cout<<"La lista está vacía"<<endl;
	}
	
	while((Mayuscula(q->postre->get_nombre()) != nombre_postre) && (band == 1)){
		if(q->sig != NULL){
			t = q;
			q = q->sig;
		} else{
			band = 0;
		}
	}
	
	if(band == 0){
		cout<<"El elemento està fuera de la lista"<<endl;
	}else {
		if(this->primero == q){
			this->primero = q->sig;
		}else{
			t->sig = q->sig;
		}
		delete q;
	}
	
}

string ListaPostre::Mayuscula(string nombre){
	// funcion que convierte los nombres en mayucula
	int j = nombre.size();
	for (int i =0; i< j ; i++){
		nombre[i] = toupper(nombre[i]);
	}
	return nombre;
}

Postre ListaPostre::buscar(string name){
	/**busca un objeto que tega el mimso nomgre que el ussario coloque 
	 * si lo encuentra lo devuelve, si no, devuelve un objeto con el 
	 * nombre e vacìo*/
	
	Nodo2 *q;
	q = this->primero;
	while((q != NULL) && (Mayuscula(q->postre->get_nombre()) != Mayuscula(name))){
		q = q->sig;
	}
	
	if(q == NULL){
		cout<<"El elemento no está en la lista"<<endl;
	Postre *p = new Postre("vacio");
		return *p;
	} else {
		return *q->postre;
	}
	
}
