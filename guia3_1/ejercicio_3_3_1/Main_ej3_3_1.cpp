#include <iostream>
#include <ctype.h>
#include <string.h>

using namespace std;
#include "Listaing.h"
#include "ListaPostre.h"
#include "Postre.h"
#include "Ingrediente.h"



int main(void){
	/**
	 * option =  nª que elegirá el usuario
	 * opciones[] = lista que guarda cada option
	 * pasos = las veces que da vuelta
	 * 
	 * se ocupa un do while para entrar a menu hasta que el usuario no
	 * aprete una opcion que esté dentro del menú
	 * 
	 * 
	 * */
	int option;
	ListaPostre *lista = new ListaPostre();
	int opciones[] = {};
	int pasos = 0;
	do{
		cout<<endl;
	
	
		cout<<"Bienvenido al recetariode postres"<<endl;
		cout<<"Por favor elija una de las opciones del menú"<<endl;
		cout<<"Mostar listado de postes\t|1|"<<endl;
		cout<<"Agregar postre\t\t\t|2|"<<endl;
		cout<<"Eliminar postre\t\t\t|3|"<<endl;
		cout<<"Selecionar postre para...\t|4|"<<endl;
		cin>>option;
	
	
		if(option == 1){
			cout<<endl;
			lista->imprimir();
			
		} else if(option == 2){
			
			/**
			 * Se pide el nombre del postre,
			 *  se crea un nuevo postre y este se agrega a la lista*/
		
			cout<<endl;
			cout<<"indique el nombre del postre"<<endl;
			cin.ignore();
			string nombre;
			
			getline(cin, nombre);
			
			Postre *p = new Postre(nombre);
			
			lista->crear_ordenada(p);
			
		} else if(option == 3){
			/**para esta opción es necesario el aray opciones[]
			 * 
			 * cont  = para contar cuantas veces a aparecido un 2
			 * el for se encarga de recorrer la lista buscando un 2
			 * 
			 * si encuentra alguno, o sea, si alguna vez se ha agregado un postre
			 * se podrá eliminar, si no, la lista se mostrará vacía
			 * 
			 * para eliminarlo solo tienne que indicar el nombre
			 * */
			int cont = 0;
			for(int i =0; i<pasos; i++){
				if(opciones[i] == 2){
					cont++;
				}
			}
			if(cont >0){
				cout<<endl;
				cout<<"indique el nombre del postre que desea eliminar"<<endl;
				cin.ignore();
				string nombre;
			
				getline(cin, nombre);
				lista->eliminar(nombre);
			}else{
				cout<<"La lista está vacía"<<endl;
			}
		} else if(option == 4){
			/**
			 *  Esta opción equiere primero un postre, entonces se le pide al usuario
			 * el nombre para buscarlo ediante la función buscar() 
			 * que va a devolver un objeto tipo postre.*/
			cout<<endl;
			//Postre *buscar_p;
			cout<<"indique el postre"<<endl;
			cin.ignore();
			string nombre_postre;
			
			getline(cin, nombre_postre);
			//*buscar_p = ;
			cout<<lista->buscar(nombre_postre).get_nombre()<<endl;
			
			if(lista->buscar(nombre_postre).get_nombre() != "vacio"){
			/**si es que lo encuentra se va a presentar el menú nuevo, se 
			 * ocupa el mismo mecanuismo que el meú original para dar vueltas*/
				int option2;
				int opciones2[] = {};
				int pasos2 = 0;
				do{
					cout<<endl;
					
					cout<<"Mostrar sus ingredientes|1|"<<endl;
					cout<<"Agregar un ingrediente\t|2|"<<endl;
					cout<<"Eliminar un ingrediente\t|3|"<<endl;
					cin>>option2;
					/**Se usa las mismas formas para mostrar, agregar o elimanr un ingrediente
					 * ocupadas para postre
					 * */
					if(option2==1){
						lista->buscar(nombre_postre).get_lista().imprimir();
					}else if(option2==2){
						cout<<endl;
						cout<<"indique el nombre del ingrediente"<<endl;
						cin.ignore();
						string nombre_ing;
			
						getline(cin, nombre_ing);
				
						cout<<"indique la cantidad del ingrediente"<<endl;
						cin.ignore();
						string cantidad_ing;
			
						getline(cin, cantidad_ing);
			
						Ingrediente *ing = new Ingrediente(nombre_ing, cantidad_ing);
			
			
						lista->buscar(nombre_postre).get_lista().crear_ordenada(ing);
					}else if(option2 == 3){
						
						int cont2 = 0;
						for(int i =0; i<pasos2; i++){
							if(opciones2[i] == 2){
								cont2++;
							}
						}
						if(cont2 >0){
							cout<<endl;
							cout<<"indique el nombre del ingediente que desea eliminar"<<endl;
							cin.ignore();
							string nombre_ing;
			
							getline(cin, nombre_ing);
							lista->buscar(nombre_postre).get_lista().eliminar(nombre_ing);
						}	else{
							cout<<"La lista está vacía"<<endl;
						}
					}else{
						option2 = 4;
						break;
					}
					opciones2[pasos2] = option2;
					pasos2 ++;
				}while (option2 != 4);
			}
			
		}else{
			cout<<endl;
			option = 5;
			
		}
		opciones[pasos] = option;
		pasos ++;
	}while(option != 5);
   
  


	delete lista;
	
	return 0;
	
  
}
