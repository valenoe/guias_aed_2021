#ifndef LISTAING_H
#define LISTAING_H

#include <iostream>
using namespace std;

#include "Ingrediente.h"

// define la estructura del nodo. 
typedef struct _Nodo {
	
	Ingrediente *ingrediente;
    struct _Nodo *sig;

} Nodo1;

class Listaing {
    private:
    //falla al comparar las estructuras sig con primero
        Nodo1 *primero = NULL;
        

    public:
        /* constructor*/
        Listaing();


        void crear_ordenada(Ingrediente *ingrediente);
        
        /* imprime la lista. */
        void imprimir ();
        
        void eliminar(string nombre);
        
        string Mayuscula(string nombre);
        
      
};
#endif
