#ifndef LISTAPOSTRE_H
#define LISTAPOSTRE_H

#include <iostream>
using namespace std;

#include "Postre.h"
// define la estructura del nodo. 
typedef struct Nodo {
	
	Postre *postre;
    struct Nodo *sig;

} Nodo2;

class ListaPostre {
    private:
        Nodo2 *primero = NULL;
        

    public:
        /* constructor*/
        ListaPostre();


        void crear_ordenada(Postre *postre);
        
        /* imprime la lista. */
        void imprimir ();
        
        void eliminar(string nombre);
        string Mayuscula(string nombre);
      
		Postre buscar(string name);
};
#endif
