#include <ctype.h>
#include <iostream>
using namespace std;

#include "Listaing.h"


Listaing::Listaing(){
}


void Listaing::crear_ordenada(Ingrediente *ingrediente){
	
	Nodo1 *temp;
	
	
	temp = new Nodo1;
	
	temp->ingrediente = ingrediente;
	temp->sig = NULL;
	
	// letra repersenta la primera letra del nombre que va a ingresar en mayúscula
	char letra = toupper(ingrediente->get_nombre()[0]);
	
	// solo se compararán los nombres de los ingredientes
	/**
	 * Se van a evaluar dos cosas:
	 *  1) Que el primero elemento esté vacío.
	 *  2) Que la primera letra del nombre que va a entrar vaya antes 
	 * 		que la del primer nombre. 
	 * 
	 * en ambos casos el objeto siguiente toma el valor del primero
	 * y primero toma el valor de temp
	 * */
	if(this->primero == NULL || letra < toupper(this->primero->ingrediente->get_nombre()[0]))
	{
		temp->sig = this->primero;
		this->primero = temp;
		
		
	}
	else{
		/**
		 * Creacion de otro nodo temporal que va a simular como el primer 
		 * objeto de la lista 
		 * */
		Nodo1 *otro_temp;
		otro_temp = this->primero;
			
		while(otro_temp->sig != NULL && toupper(otro_temp->sig->ingrediente->get_nombre()[0]) < letra){
			/**
			 * Este while no va a funcionar cuando:
			 *  1) El nodo siguiente sea NULL, esto es para no seguir 
			 * 		avanzando si llega al final
			 *  2) El nodo siguente va después que el nombre que se va a ingresar
			 *  
			 * si no se cumplen ninguna de estas condiciones, enonces 
			 * otro_temp va a tomar el valor siguiente
			 * */
			otro_temp = otro_temp->sig;
		}
		
		if(otro_temp->sig != NULL){
			/** si la condicion para que el while se rompiera es que 
			 * otro_temp->sig va después que el nombre que se quiere ingresar
			 * pero no estaba al final de la lista, entonces temp->sig va a 
			 * apuntar a otro_temp->sig.
			 * 
			 * */
			temp->sig = otro_temp->sig;
		
		}
	
		
		/**
		 * Otro temp->sig siempre va a terminar apuntado a temp
		 * */
		otro_temp->sig = temp;
		
		
		
	}
	cout<<"\nSe ha agregado unnuevo nombre: "<<endl;
	cout<<"Estado actual de la lista"<<endl;
	imprimir();
	
}

void Listaing::imprimir(){
	/**
	 * En esta funcoón se crea un nuevo nodo temporal que va a simular 
	 * el primer objeto de la lista.
	 * Este se va a usar para imprimir los objetos de la lista mediante un ciclo while
	 * 
	 * while: 
	 * 		Se repetirá hasta que se encuetre con un espacio vacio.
	 * 		Imprimirá el valor del nombre perteneciente a temp y luego temp
	 * 		va a apuntar al espacio siguiente.
	 *  */
	
	Nodo1 *temp = this->primero;
	
	if(temp == NULL){
		cout<<"La lista está vacía"<<endl;
	}
	
	while(temp != NULL){
		cout<<" el ingrediente es:"<<temp->ingrediente->get_nombre()<<endl;
		temp = temp->sig;
	}
	
	
}

void Listaing::eliminar(string nombre){
	
	/**para eliminar se necesitan 2 nodos auxiliares, 
	 * uno que va a recorre la lista y otro que lo reemplazará en caso
	 * de encontrar el objeto y que este no sea el primero*/
	Nodo1 *q, *t;
	int band = 1;
	q = this->primero;
	string nombre_postre = Mayuscula(nombre);
	
	if(q == NULL){
		cout<<"La lista está vacía"<<endl;
	}
	
	while((Mayuscula(q->ingrediente->get_nombre()) != nombre_postre) && (band == 1)){
		if(q->sig != NULL){
			t = q;
			q = q->sig;
		} else{
			band = 0;
		}
	}
	
	if(band == 0){
		cout<<"El elemento està fuera de la lista"<<endl;
	}else {
		if(this->primero == q){
			this->primero = q->sig;
		}else{
			t->sig = q->sig;
		}
		delete q;
	}
	
}

string Listaing::Mayuscula(string nombre){
	// funcion que convierte los nombres en mayucula
	int j = nombre.size();
	for (int i =0; i< j ; i++){
		nombre[i] = toupper(nombre[i]);
	}
	return nombre;
}
