#include <iostream>
using namespace std;

#include "Ingrediente.h"

Ingrediente::Ingrediente( string nombre, string cantidad){
	this->nombre = nombre;
	this->cantidad = cantidad;
}


string Ingrediente::get_nombre(){
	return this->nombre;
	
}

string Ingrediente::get_cantidad(){
	return this->cantidad;
	
}

void Ingrediente::set_nombre(string nombre){
	this->nombre = nombre;
}

void Ingrediente::set_cantidad(string cantidad){
	this->cantidad = cantidad;
}
