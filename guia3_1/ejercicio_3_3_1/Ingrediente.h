#ifndef INGREDINETE_H
#define INGREDINETE_H

#include <iostream>
using namespace std;





class Ingrediente{
    private:
        string nombre;
        string cantidad;

    public:
        /* constructor*/
        Ingrediente(string nombre, string cantidad);
        
        string get_nombre();
        string get_cantidad();
        
        void set_nombre(string nombre);
        void set_cantidad(string cantidad);
        
        
       
};
#endif

