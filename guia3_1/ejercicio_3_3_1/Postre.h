#ifndef POSTRE_H
#define POSTRE_H

#include <iostream>
using namespace std;

#include "Ingrediente.h"
#include "Listaing.h"



class Postre {
    private:
        string nombre;
        Listaing *ingredientes;

    public:
        /* constructor*/
        Postre(string nombre);
        void set_nombre(string nombre);
        string get_nombre();
        Listaing get_lista();
       
};
#endif

