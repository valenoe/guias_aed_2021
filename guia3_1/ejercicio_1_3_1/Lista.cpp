#include <iostream>
using namespace std;

#include "Lista.h"

Lista::Lista(){
}

void Lista::crear_desordenada(int numero) {
    Nodo *tmp;
    tmp = new Nodo;
    tmp->numero = numero;
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como primero y como último nodo. */
    if (this->primero == NULL) { 
        this->primero = tmp;
        this->ultimo = this->primero;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo 
    como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}
void Lista::crear_ordenada(int numero){
	
	Nodo *temp;
	
	
	temp = new Nodo;
	
	temp->numero = numero;
	temp->sig = NULL;
	
	
	/**
	 * Se van a evaluar dos cosas:
	 *  1) Que el primero elemento esté vacío.
	 *  2) Que el numero que va a entrar sea menor que el primero. 
	 * 
	 * en ambos casos el objeto siguiente toma el valor del primero
	 * y primero toma el valor de temp
	 * */
	if(this->primero == NULL || numero < this->primero->numero)
	{
		temp->sig = this->primero;
		this->primero = temp;
		
		
	}
	else{
		/**
		 * Creacion de otro nodo temporal que va a simular como el primer 
		 * objeto de la lista 
		 * */
		Nodo *otro_temp;
		otro_temp = this->primero;
			
		while(otro_temp->sig != NULL && otro_temp->sig->numero < numero){
			/**
			 * Este while no va a funcionar cuando:
			 *  1) El nodo siguiente sea NULL, esto es para no seguir 
			 * 		avanzando si llega al final
			 *  2) El nodo siguente es mayor al número que se va a ingresar
			 *  
			 * si no se cumplen ninguna de estas condiciones, enonces 
			 * otro_temp va a tomar el valor siguiente
			 * */
			otro_temp = otro_temp->sig;
		}
		
		if(otro_temp->sig != NULL){
			/** si la condicion para que el while se rompiera es que 
			 * otro_temp->sig es mayor al numero que se quiere ingresar
			 * pero no estaba al finalde la lista, entonces temp->sig va a 
			 * apuntar a otro_temp->sig.
			 * 
			 * */
			temp->sig = otro_temp->sig;
		
		}
	
		
		/**
		 * Otro temp->sig siempre va a terminar apuntado a temp
		 * */
		otro_temp->sig = temp;
		
		
		
	}
	
	
}

void Lista::imprimir(){
	/**
	 * En esta funcoón se crea un nuevo nodo temporal que va a simular 
	 * el primer objeto de la lista.
	 * Este se va a usar para imprimir los objetos de la lista mediante un ciclo while
	 * 
	 * while: 
	 * 		Se repetirá hasta que se encuetre con un espacio vacio.
	 * 		Imprimirá el valor de numero perteneciente a temp y luego temp
	 * 		va a apuntar al espacio siguiente.
	 *  */
	
	Nodo *temp = this->primero;
	
	while(temp != NULL){
		cout<<" el numero es:"<<temp->numero<<endl;
		temp = temp->sig;
	}
}


void Lista::repartir(int codigo, Lista *l_original){
	/**
	 * La función ocupa un nodo temporal que apunta al primer objeto de 
	 * la lista original
	 * 
	 * El while va a recorrer esta lista y, dependiendo del codigo, va 
	 * a llenar la lista con números positivos o negativos
	 * */
	Nodo *temp = l_original->primero;
	
	// codigo:
	// 		1 = positivos
	// 		2 = negativos
	while(temp != NULL){
		if(codigo == 1){
			if(temp->numero >=0){
				crear_ordenada(temp->numero);
				
			}
		} else if(codigo == 2){
			if(temp->numero < 0){
				crear_ordenada(temp->numero);
				
			}
		}
		temp = temp->sig;
		
	}
}
