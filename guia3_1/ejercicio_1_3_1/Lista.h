#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;


// define la estructura del nodo. 
typedef struct _Nodo {
	
	int numero;
    struct _Nodo *sig;

} Nodo;

class Lista {
    private:
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
       
        void crear_desordenada(int numero);
        void crear_ordenada(int numero);
        void repartir(int codigo, Lista *l_original);
        /* imprime la lista. */
        void imprimir ();
        
      
};
#endif
