 #include <iostream>
 #include <time.h>
using namespace std;
#include "Lista.h"



int main(){
	 srand(time(NULL));
   
   
   Lista *lista = new Lista();
   Lista *L_positiva = new Lista();
   Lista *L_negativa = new Lista();
	
	int tamanio, aleatorio, DESDE=-4, HASTA=4;
	cout<<"Ingrese la cantidad de elementos que va a querer en la lista"<<endl;

	cin>>tamanio;
	
	for(int i =0; i<tamanio; i++){
		aleatorio = rand()%(HASTA-DESDE+1)+DESDE;
		lista->crear_desordenada(aleatorio);
	}
	
	cout<<"\nLista original"<<endl;
	lista->imprimir();
	
	// lleva el codigo y la lista a repartir
	L_positiva->repartir(1, lista);
	L_negativa->repartir(2, lista);
	
	cout<<"\nLista positiva"<<endl;
	L_positiva->imprimir();
	cout<<"\nLista negativa"<<endl;
	L_negativa->imprimir();

	delete lista;
	delete L_positiva;
	delete L_negativa;

	return 0;
  
}
