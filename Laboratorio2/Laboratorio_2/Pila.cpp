#include <iostream>

using namespace std;

#include "Pila.h"

// Constructor de la clase

Pila::Pila(int max){
	this->max = max;
	this->datos = new int[max];
	this->tope = 0;
	
	
}

// Métodos específicos del problema
void Pila::pila_vacia(){
	
	if(tope == 0){
		this->band = true;
	}
	else{
		this->band = false;
	}
}


void Pila::pila_llena(){
	
	if(tope == max){
		this->band = true;
	}
	else{
		this->band = false;
	}
}

void Pila::push(int dato){
	pila_llena();
	if(band){
		cout<<"Desbordamiento, pila llena"<<endl;
	}
	else{
		this->tope = tope + 1;
		datos[tope] = dato;
	}
}

int Pila::pop(){
	pila_vacia();
	
	if(band){
		cout<<"Subdesbordamiento, Pila vacía"<<endl;
		return 0;
	}
	else{
		int dato = datos[tope];
		this->tope = tope - 1;
		
		return dato;
	}
}


// Metodos get de la clase, no set debido a que los datos, como tope, 
// band o max, no se pueden editar después de creados
int Pila::get_datos(int i){
	return this->datos[i];

}

bool Pila::get_band(){
	return this->band;
}

int Pila::get_tope(){
	return this->tope;
}


	
