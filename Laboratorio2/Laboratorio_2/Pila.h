#ifndef PILA_H
#define PILA_H


class Pila{
	
	private:
		int max;
		int tope;
		int *datos = NULL;
		bool band;
		
	public:
		// Constructor de la clase
		Pila(int max);
		// Métodos específicos del problema
		void pila_vacia();
		void pila_llena();
		void push(int dato);
		int pop();
		
		// Metodos get de la clase
		int get_datos(int i);
		bool get_band();
		int get_tope();
		
	

};
#endif
