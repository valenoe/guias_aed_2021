#include <iostream>

using namespace std;

#include "Pila.h"

int menu(Pila pila, int max){
	/**
	 * Función menu: muestra las opciones que tiene el usuario para
	 * editar la pila
	 * 
	 * Mediante un el uso de un switch se cumple lo pedido por el usuario
	 * 
	 * -	case 1: "Agregar/push"  
	 * 
	 * 			Se crea la variable "valor" para recibir el número que se 
	 * 			quiera ingresar.
	 * 
	 * 			Se llama a la función push() para agreggar el dato.
	 * 
	 * 			Todo esto dentro de un ciclo while que solo se va a detener
	 * 			cuando la pila esté llena, para saber el estado de la pila 
	 * 			se llama a la función pila_llena() que modifica el valor 
	 * 			de band, true significa lleno y false, vacío. 
	 * 
	 * -	case 2: "Remover/pop"
	 * 
	 * 			Se pregunta si tope vale 0, sin importar la respuesta igual
	 * 			se va a llamar a pop() que retira el último elemnto, pero 
	 * 			se realiza esta pregunta para que las impesiones calcen
	 * 			ya que si la respuesta es sí, se va a imprimir "Subdesbordamiento, 
	 * 			Pila vacía", que sale directamente desde la función pop(),
	 * 			pero si la respuesta es no, pop() solo devuelve un número.
	 * 			 
	 * -	case 3: "Ver Pila"
	 * 
	 * 			Para imprimir la pila se necesita el valor actual de tope,
	 * 			este se obtiene con la función get_tope().
	 * 			
	 * 			Mediante un ciclo for se va imprimiendo la pila desde su 
	 * 			último objeto hasta el primero.
	 * 
	 * -	case 0: "Salir"
	 * 
	 * 			return 0 termina inmediatamente el programa.
	 * 
	 * -	default:
	 * 			Esta opción esta en caso de que no se elija ninguna de 
	 * 			las anterirores.
	 * 			
	 * 			Hará lo mismo que case 0.
	 * 
	 * Esta función es recursiva debido que al finalizar el switch se llama
	 * nuevamente a menu(pila, max), lo que produce que se repita todo
	 * hasta que el usuario ingrese 0 o cualquier otro número que no esté 
	 * en el menú.
	 * */
	
	cout<<"\nAgregar/push\t[1]"<<endl;
	cout<<"Remover/pop\t[2]"<<endl;
	cout<<"Ver Pila\t[3]"<<endl;
	cout<<"Salir\t\t[0]"<<endl;
	
	int opcion;
	cout<<"-------------------\nOpción: "; 
	cin>>opcion;
	cout<<endl;
	switch(opcion){
		case 1:
			while(true){
				int valor;
				cout<<"Ingrese valor: ";
				cin>>valor;
				pila.push(valor);
				pila.pila_llena();
				if(pila.get_band()){
					break;
				}
			}
			break;
		case 2:
			if(pila.get_tope() == 0){
				pila.pop();
			}
			else{
				cout<<"Elemento eliminado "<<pila.pop()<<endl;;
			}
			break;
		case 3:
			for(int i=pila.get_tope(); i>0; i--){
				cout<<"|"<<pila.get_datos(i)<<"|"<<endl;
			}
			break;
		case 0: 
			return 0;
		default:
			cout<<"Esta opción no está en el menú, de todas formas saldrá"<<endl;
			return 0;		
	}
	menu(pila, max);
	return 0;
}

int main(void){
	/**
	 * La función main permite ingresar el valor máximo de la pila y crearla
	 * 
	 * Luego se llama a la función menú para poder agregar, quitar o mostrar
	 * los elementos que contiene
	 * */
	int max;
	cout<<"Ingrese el tamaño máximo de la pila ";
	cin>>max;
	
	Pila pila = Pila(max);
	
	menu(pila, max);
	
	return 0;
}
