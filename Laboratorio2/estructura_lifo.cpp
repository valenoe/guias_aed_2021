#include <iostream> 
#include <stack>

using namespace std;

int main(){
	string parentesis;
	stack<char>pila;
	cin>>parentesis;
	
	int i, largo = parentesis.length();
	
	
	for(i=0; i < largo; i++){
		if(parentesis[i] == '('){
			pila.push('(');
		}
		else{
			if(pila.empty()){
				cout<<"No està balanceada\n";
				return 0;
			}
			pila.pop();
			
		}
		
	}
	
	if(pila.empty()){
		cout<<"Està balanceada\n";
	}
	
	return 0;
}

