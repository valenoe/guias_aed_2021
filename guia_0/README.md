Instrucciones guia_0

1) Compilar al archivo Makefile usando el comando make
2) Iniciar el programa con el comando ./programa
3) Se le presentará un menù con 3 opciones:
	1.- Ingresar una proteína.
	2.- Imprimir las proteínas. 
	3.- Salir del programa.
  Si escoge cualquier otra ocpcion numerica volvera al menu inicial.
4) Explicación opciones.
	1.- Ingresar una proteina: lo va a llevar a una función que le va a pedir los datos en
	forma secuencial, primero proteína, luego cadena, luego aminoácidos, luego átomos y 
	finalmente coordenadas, se preguntarà tambien por la cantidad de cadenas, aminoácidos y 
	átomos para crear ciclos y poder preguntar por todos los datos.
		Proteínas: pedirá el nombre, el Id y la contidad de cadenas.
		Cadenas: pedirá la letra y la cantidad de aminoácidos.
		Aminoácidos: pedirá el nombre, el número y la cantidad de átomos.
		Atomos: pedirá el nombre, el número y las coordenadas, estas últimas son 3: x, y, z. 
	Después de tener todos los datos se gurdará la proteína en una lista principal de proteínas.
	
	 2.- Imprimir las proteínas: esto lo llevará a otra funcion encargada de imprimir todos
	 los datos ingresados anteriormente, identificando cada uno de los objetos.
	 Solo se llevará a cabo si es que hay alguna proteina dentro de la lista principal.
	 
	 3.- Salir: termina el ciclo con el cual se mostraba el menú.
	  
