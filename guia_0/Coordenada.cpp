#include <iostream> 
using namespace std;

#include "Coordenada.h"

// Constructores de la clase 
Coordenada::Coordenada(){
    
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
}

Coordenada::Coordenada (float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
        
// métodos get and set 
float Coordenada::get_x(){
	return this->x;
}
		
float Coordenada::get_y(){
	return this->y;
}
        
float Coordenada::get_z(){
	return this->z;
}
        
        
        
        void Coordenada::set_x(float x){
			this->x = x;
		}
        
        void Coordenada::set_y(float y){
			this->y = y;
		}
        
        void Coordenada::set_z(float z){
			this->z = z;
		}
		
		
        
/*


int main (int argc, char **argv) {
	Coordenada c = Coordenada(1, 2, 3);
	cout<<"x: "<<c.get_x();
}

*/
