
#include <iostream>
#include <string>
#include <list>


using namespace std;
// Se incluyen los archivos .h para poder usar sus funciones
#include "Atomo.h"
#include "Coordenada.h"
#include "Aminoacido.h"
#include "Cadena.h"
#include "Proteina.h"


using namespace std;

Proteina leer_datos_proteina(){
	/*Esta función se llama cuando se debe ingresar una nueva proteína
	 *Se inicia como Proteina porque devuelve un objeto tipo Proteina
	 * 
	 * pasos que sigue la funcion:
	 * 1.- Pedir el nombre e Id de la proteina al usuario, con ellos crea el objeto
	 * 2.- Preguntar por la contidad de cadenas
	 * 3.- Dentro del ciclo de las cadenas, pedir su letra y crear el objeto Cadena
	 * 4.- Preguntar por la cantidad de aminoácidos que tiene la cadena
	 * 5.- Dentro del ciclo de los aminoácidos, pedir su nombre y número 
	 * 		para cerar cada objeto Aminoacido
	 * 6.- Preguntar por la cantidad de átomos que tiene cada aminoácido
	 * 7.- Dentro del ciclo de los atomos, pedir el nombre, el número y las coordenadas
	 * 		Con las coordenadas se crea el objeto Coordenada, y con este se crea el objeto Atomo
	 * 8.- Al final de cada ciclo for se agregan los objetos a sus listas correspondientes
	 * 
	 * Finalmente se devuelve la proteína creada al princio de la función.
	 * Esta función está hecha para ser llamada dentro de un menú repetitivo
	 * 
	 * */
	 
	
	 
	string nombre, id;
	int num_cadenas, i;
	cin.ignore();
	cout<<"Proteína"<<endl;
	cout<<"Nombre: ";
	getline(cin, nombre);
	//cout<<"nombre: "<<nombre<<endl;
	
	
	cout<<"Id: ";
	cin.ignore();
	getline(cin,id);
	
	
	Proteina proteina = Proteina(nombre, id);
	
	cout<<"Ingrese el número de cadenas que tiene la proteína"<<endl;
	cin>>num_cadenas;
	for(i = 0; i < num_cadenas; ++i){
		string letra;
		int num_aa, j;
		cout<<"Cadena "<<i+1<<endl;
		cout<<"Letra: ";
		cin.ignore();
		getline(cin,letra);
		Cadena cadena = Cadena(letra);
		

		
		cout<<"Ingrese el número de aminoácidos que tiene la cadena"<<endl;
		cin>>num_aa;
		for(j=0; j<num_aa;++j){
			string nombre_a;
			int num_a, num_atomos, k;
			cout<<"Aminoácido "<<j+1<<endl;
			cout<<"Nombre: ";
			cin.ignore();
			getline(cin,nombre_a);
			cout<<"Número: ";
			cin>>num_a;
			
			Aminoacido aminoacido = Aminoacido(nombre_a, num_a);
			cout<<"Ingrese el número de átomos que tiene el Aminoacido"<<endl;
			cin>>num_atomos;
			
			
			for(k = 0; k < num_atomos; ++k){
				string nombre_atomo;
				int numero_atomo, x, y, z;
				cout<<"Átomo "<<k+1<<endl;
				cout<<"Nombre: ";
				cin.ignore();
				getline(cin,nombre_atomo);
				cout<<"Número: ";
				cin>>numero_atomo;
				cout<<"A continuación se le pedirán las coordenadas: "<<endl;
				cout<<"Posición x: ";
				cin>>x;
				cout<<"Posición y: ";
				cin>>y;
				cout<<"Posición z: ";
				cin>>z;
				
				Coordenada coordenada = Coordenada(x, y, z);
				Atomo atomo = Atomo(nombre_atomo, numero_atomo, coordenada);
				aminoacido.add_atomo(atomo);	
			}
			cadena.add_aminoacido(aminoacido);	
		}
		proteina.add_cadena(cadena);
	}
	return proteina;
}

void imprimir_datos_proteina(list<Proteina> proteinas){
	/*
	 * Esta función recive la lista de proteínas creada en el main
	 * Se utilizan ciclos for para recorrer las listas de Proteinas, 
	 * cadenas, Aminoacidos y Atomos
	 * 
	 * El for está creado para identificar un objeto de una clase en 
	 * especifico dentro de una lista e imprimir sus atributos
	 * 
	 * Se usan for anillados debido a que cada clase tiene listas en su interiror
	 * */
	 
	int a=0;
	for(Proteina prot: proteinas){
		++a;
		int b=0;
		cout<<"········································Datos Proteinas··········"<<endl;
		cout<<endl;
		cout<<"Proteína: "<<a<<endl;
		cout<<"Nombre Proteina: "<<prot.get_nombre()<<endl;
		cout<<"Id Proteina: "<<prot.get_id()<<endl;
		cout<<"Numero de cadenas de la Proteina: "<<prot.get_cadenas().size()<<endl;
		cout<<endl;
		
		for(Cadena cade: prot.get_cadenas()){
			++b;
			int c=0;
			cout<<"······························Datos de las cadenas··········"<<endl;
			cout<<endl;
			cout<<"Cadena: "<<b<<endl;
			cout<<"Letra de la cadena: "<<cade.get_letra()<<endl;
			cout<<"Numero de aminoácidos de la cadena: "<<cade.get_aminoacidos().size()<<endl;
			cout<<endl;
			
			for(Aminoacido AA: cade.get_aminoacidos()){
				
				++c;
				int d=0;
				cout<<"····················Datos de los aminoacidos··········"<<endl;
				cout<<endl;
				cout<<"Aminoácido: "<<c<<endl;
				cout<<"Nombre del Aminoácido: "<<AA.get_nombre()<<endl;
				cout<<"Numero del Aminoácido: "<<AA.get_numero()<<endl;
				cout<<"Numero de átomos del aminoácido: "<<AA.get_atomos().size()<<endl;
				cout<<endl;
				
				
				for(Atomo atom: AA.get_atomos()){
					++d;
					cout<<"··········Datos de los átomos··········"<<endl;
					cout<<endl;
					cout<<"Átomo: "<<d<<endl;
					cout<<"Nombre del Atomo: "<<atom.get_nombre()<<endl;
					cout<<"Numero del Atomo: "<<atom.get_numero()<<endl;
					cout<<"Coordenadas del Atomo: ("<<atom.get_coordenada().get_x();
					cout<<", "<<atom.get_coordenada().get_y();
					cout<<", "<<atom.get_coordenada().get_z()<<") "<<endl;
					cout<<"·······································"<<endl;	
				}
			}
		}
		
		cout<<"·······························"<<endl;	
	}
}


int main(){
	
	// creacion de la lista principal
	
	list<Proteina> proteinas;

	
//	imprimir_datos_proteina(proteinas);
	// bandera pertenece al while y optian al menú
	int bandera = 0, option;
	while(bandera == 0){
		/*Mientras que la bandera sea 0 el ciclo seguirá ejecutandose*/
		cout<<"Igrese la opción que desea"<<endl;
		cout<<"Ingresar una proteína  ---> 1"<<endl;
		cout<<"Imprimir las proteínas ---> 2"<<endl;
		cout<<"Salir del programa     ---> 3"<<endl;
		cin>>option;
		cout<<"--------------------------------------------"<<endl;
		if(option == 1)
		{	
			/*
			 * La funcion leer_datos_proteina() está hecha para devolver
			 * un objeto Proteina, entonces al momento de terminar la función
			 * se añadirá el objeto a la lista principal
			 * */
			proteinas.push_back(leer_datos_proteina());
		
		}
		else if(option == 2)
		{
			/*
			 * La opción 2 es para imprimir la lista principal
			 * siempre y cuando haya algo en su interior
			 * */
			if(proteinas.size() == 0)
			{
				cout<<"No hay proteinas ingresadas"<<endl;
			}
			else
			{
				cout<<endl<<endl;
				imprimir_datos_proteina(proteinas);
			}
		
		}
		else if(option == 3)
		
		{
			/*Bandera solo de vuelve 0 cuando option vale 3*/
			bandera =1;
		}
		else
		{
			cout<<"Esta opcion no está en el menú"<<endl;
		}
		cout<<"--------------------------------------------"<<endl;
		
	}
	cout<<"Se cerrará el programa"<<endl;
	
	
	return 0;

}

