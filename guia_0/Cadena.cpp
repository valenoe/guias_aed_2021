
#include <iostream>

using namespace std;


#include "Cadena.h"
#include <list>


// Constructor de la clase
Cadena::Cadena(string letra){
	this->letra = letra;
}

// Metodos set y get de la clase
string Cadena::get_letra(){
	return this->letra;
}

list<Aminoacido> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}

void Cadena::set_letra(string letra){
	this->letra = letra;
}

void Cadena::add_aminoacido(Aminoacido aminoacido){
	this->aminoacidos.push_back(aminoacido);
}
