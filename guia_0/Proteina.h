#ifndef PROTEINA_H
#define PROTEINA_H

#include <list>
//Se llama a Cadena.h para poder usarlo como lista
#include "Cadena.h"

class Proteina{
	private: 
		string nombre;
		string id;
		list<Cadena> cadenas;
		
	public:
		// constructor
		Proteina(string nombre, string id);
		
		// métodos get and set 
		string get_nombre();
		string get_id();
		list<Cadena> get_cadenas();
		
		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadena(Cadena cadena);
		
};
#endif
		
	
