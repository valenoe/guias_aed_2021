#ifndef AMINOACIDO_H
#define AMINOACIDO_H

#include <list>
//Se llama a Atomo.h para poder usarlo como lista
#include "Atomo.h"


class Aminoacido {
    private:
        string nombre;
        int numero;
        list<Atomo> atomos;
        
    public:
        // constructor
        Aminoacido(string nombre, int numero);

        
        // métodos get and set 
		string get_nombre();
        int get_numero();
        list<Atomo> get_atomos();
       
       
        void set_nombre(string nombre);
        
        void set_numero(int numero);
        
        void add_atomo(Atomo atomo);
		
		
        
};
#endif 
