#ifndef CADENA_H
#define CADENA_H

#include <list>
//Se llama a Aminoacido.h para poder usarlo como lista
#include "Aminoacido.h"

class Cadena{
	
	private:
		string letra;
		list<Aminoacido> aminoacidos;
		
	public:
		// constructor
		Cadena(string letra);
		
		
		// métodos get and set 
		string get_letra();
		list<Aminoacido> get_aminoacidos();
		
		void set_letra(string letra);
		void add_aminoacido(Aminoacido aminoacido);
};
#endif		
