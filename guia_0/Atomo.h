
#ifndef ATOMO_H
#define ATOMO_H
#include <iostream>
#include <string>
#include "Coordenada.h"

using namespace std;


class Atomo{
	
	private:
		string nombre = "\0";
		int numero = 0;
		Coordenada coordenada;
		
	public:
		// constructor
		Atomo(string nombre, int numero, Coordenada coordenada);
		
		
		// métodos get and set 
		string get_nombre();
		int get_numero();
		Coordenada get_coordenada();
		
		void set_nombre( string nombre);
		void set_numero(int numero);
		void set_coordenada(Coordenada coordenada);
		
		
	
};
#endif 
	
