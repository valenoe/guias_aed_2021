#include <iostream>

using namespace std;

#include "Arbol.h"

Arbol::Arbol(){
}


Nodo *Arbol::crear_nodo(int num){
	Nodo *nodo = new Nodo();
	nodo->der = NULL;
	nodo->izq = NULL;
	nodo->info = num;
	return nodo;

}

void Arbol::crear_arbol(Nodo *raiz){
	this->raiz = raiz;
}

void Arbol::insertar_numero(Nodo *&nodo, int num){
	
	/**
	 * Esta función se encarga de insertar números dependiendo si son
	 * mayores o menores al primer número insertado.
	 * 
	 * la función recorre el arbol buscando un hueco para el nuevo numero.
	 * solo revisa 1 nodo, lo que está a su izquierda y lo que está 
	 * a su derecha, si eso tres tienen un numero asignado se va a llamar
	 * recursivamente desdeel último que podía calzar con el nuevo número.
	 * */
	if(nodo == NULL){
		
		// solo va a estar vacío la primera vez, con la raiz
		nodo = crear_nodo(num); 
		
	} else {
		if(num < nodo->info){
			
			
			if(nodo->izq == NULL){
				nodo->izq = crear_nodo(num);
				
			} else {
				insertar_numero(nodo->izq, num);
			}
			
		} else {
			if(num > nodo->info){
				
				if(nodo->der == NULL){
					nodo->der = crear_nodo(num);
					
				} else {
					insertar_numero(nodo->der, num);
				}
			} else {
				cout<<"El número ya está en el arbol"<<endl;
			}
		}
	}
	
}

void Arbol::eliminar_numero(Nodo *&nodo, int num){
	/**
	 * esta función tiene dos posibles resultados:
	 *  1) encontrar el numero bucado y eleiminarlo.
	 *  2) que el numero no esté.
	 * 
	 * para lograr algun de las dos recorre las ramas dependiendo del
	 * numero que se busque, no  va a recorre todas las ramas.
	 * 
	 * la primera condicion es que al arbol no esté vacio, 
	 * si eso se cumple se va a preguntar si el numero es menor o no.
	 * 
	 * Si es menor o mayor que la información del nodo actual, se va a 
	 * llamar a la función de forma recursiva con el nodo de la izquierda
	 * o derecha respectivamente.
	 * 
	 * si se recorrió la rama correspondiente y resulta estar vacío el nodo
	 * actual, se cumple el resultado n° 2.
	 * 
	 * si no, y el valor no es mayor ni menor al resultado, es porque
	 * encontró al número y va a comenzar la eliminación.
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * */
	
	if(nodo != NULL){
		if(num < nodo->info){
			eliminar_numero(nodo->izq, num);
		}
		else{
			if(num > nodo->info){
				eliminar_numero(nodo->der, num);
			}
			else{
				//proceso de eliminación
				Nodo *otro = nodo;
				if(otro->der == NULL){
					nodo = otro->izq;
				}
				else {
					if(otro->izq == NULL){
						nodo = otro->der;
					}
					else{
						
						/**
						 * si el nodo tiene derecha e izquierda con otros
						 * números, no NULL, entonces se va a buscar
						 * el nodo que esté más a la derecha del que está 
						 * a la izquierda para reemplazar al que se va a 
						 * eliminar.
						 * 
						 * si es que está a la izquierda (aux) tiene 
						 * al menos un número a la derecha, con el ciclo 
						 * while se va a mover aux hasta llegar al nodo 
						 * que esté mas a la derecha, bo va a ser verdadero.
						 * 
						 * si no, aux se queda con la inforación actual, 
						 * bo va a ser falso.
						 * 
						 * nodo->info va a cabiar por aux->info
						 * 
						 * aux1 es la raiz del que se selecciona 
						 * para reemplpazar al eliminado. Cuando se elimina 
						 * el numero buscado, a aux1 se le indica que 
						 * reemplace el nodo que perdio por NULL, 
						 * dependiendo el valor de bo. 
						 * */
						Nodo *aux = nodo->izq;
						Nodo *aux1 = NULL;
						bool bo = false;
						while(aux->der != NULL){
							aux1 = aux;
							aux = aux->der;
							bo = true;
						}
						nodo->info = aux->info;
						otro = aux;
						if(bo){
							aux1->der = aux->izq;
						}
						else{
							nodo->izq = aux->izq;	
						}	
					} 
				}
				delete(otro);
			}
		}
	}
	else{
		cout<<"La informacíon a eliminar no se encuentra en el ́arbol"<<endl;
	}
}

int Arbol::buscar_numero_a_inercambiar(Nodo *nodo, int num){
	/**
	 * Esta funcion es necesaria para intercambiar números, 
	 * va a devolver un n° que indicará si el n° buscado está en 
	 * el arbol o no:
	 * 		0 = no
	 * 		2 = sí
	 * 
	 * La función se encarga de recorrer el arbol dependiendo si el n°
	 * es menor o mayor a las ramas, como esrecursiva el n° se tiene
	 * que ir actualizando después de cada rama
	 * */
	
	
	int condicion = 0;
	cout<<"número buscado: "<<num<<endl;
	if(nodo != NULL){
		if(num < nodo->info){
			if(nodo->izq == NULL){
				
				cout<<"El número no se encuentra, \nsí se puede colocar"<<endl;	
			}
			else{
				condicion = buscar_numero_a_inercambiar(nodo->izq, num);
				return condicion;
			}
			return condicion;
			
		}
		else{
			if(num > nodo->info){
				if(nodo->der == NULL){
					cout<<"El número no se encuentra, \nsí se puede colocar"<<endl;
					
				}
				else{
					condicion = buscar_numero_a_inercambiar(nodo->der, num);
					
					return condicion;
				}
			}
			else{
				cout<<"El numero sí se encuentra en el arbol"<<endl;
				cout<<"No se puede colocar"<<endl;
					condicion = 2;
					return condicion;
					
				
			}	
			
		}
	}
	return condicion;
}

void Arbol::modificar_numero(Nodo *raiz, Nodo *&nodo, int num_original, int num_nuevo){


	/**
	 * Modificar un n°, para eso va a ir recorriendo las ramas 
	 * hasta encontra el número original, cuando lo encuentre se va a 
	 * evaluar si es una hoja, si es así, se va a llamar a la función verificar
	 * que se encarga de ver que el n° nuevo calce en la rama. verificar_hoja 
	 * va a depender de eso.
	 * 
	 * el bool verificar_hoja también va a ser verdadero cuando el n° original 
	 * no sea una hoja
	 * 
	 * si verificar_hoja es verdadero se van a llamar a otras funciones para
	 * comprobar que el n° nuevo calce con el resto de la rama hacia abajo.
	 * 
	 * si verificar_hoja es falso,se va a imprimir que no se puede
	 * 
	 * */
	 

	bool verificar_hoja = false;
	if(nodo != NULL){
		cout<<"no null, nodo: "<<nodo->info<<endl;
		if(num_original < nodo->info){
			cout<<" menor, nodo: "<<nodo->info<<endl;
			modificar_numero(raiz, nodo->izq, num_original, num_nuevo);
			
		}
		else{
			if(num_original > nodo->info){
				cout<<" mayor, nodo: "<<nodo->info<<endl;
				modificar_numero(raiz, nodo->der, num_original, num_nuevo);
			}
			
			
			else{
				// encontró al n° original
				if(nodo->der == NULL && nodo->izq == NULL){
				
					if(verificar(raiz, num_original, num_nuevo)){
						verificar_hoja = true;
					}
						
				} else {
					verificar_hoja = true;
				}
				
				if(verificar_hoja){
				
					
					bool der = true,  izq =true;
					/**
					 * esta funciones son para comprobar que el n° que va a reemplazar
					 * al otro calce con la rama, que no sea mayor que el menor de la derecha
					 * ni que sea menor que el mayor de la izquuierda
					 * 
					 * 
					 * mientras los booleanos de arriba sean verdaderos, se va a poder
					 * hacer el intercambio
					 * */
					if(nodo->der != NULL){
					
						der = menor_de_los_mayores(nodo->der, num_nuevo);
					
					
					}
					if(nodo->izq != NULL){
						izq = mayor_de_los_menores(nodo->izq, num_nuevo);
			
					}

					if(izq && der){
						cout<<"Si se puede hacer el cambio"<<endl;
						nodo->info = num_nuevo;
					
					}
				}
				else {
					cout<<"No puede colocar este número porque cambiaría el orden"<<endl;
				}
				
			}
		}
	}
	else{
		cout<<"La información a modificar no se encuentra en el ́arbol"<<endl;
	}
	
}

bool Arbol::verificar(Nodo *raiz, int num_original, int num_nuevo){
	bool intercambio= false;
	bool hoja =false;
	
	/**verificar usa como base la función insertar numero, solo que cuando
	 * encuentra un hueco para ingresar un n° nuevo guarda la raiz de este, 
	 * tambien evalua si es una hoja, ya que solo nos importa en este caso para 
	 * esta función
	 * 
	 * cuando encuentre la hoja correspondiente, se va a evaluar que 
	 * la información que contennga sea la misma que la del n° original, 
	 * si es así, entonces el número nuevo no va a cambiar el orden del arbol.
	 * 
	 * para informar de esto a la función modificar_numero se usa un bool intercambio,
	 * este va a ser verdadero cuanod sea viable el intercambio
	 * 
	 * */
	
	if(num_nuevo < raiz->info){		
		if(raiz->izq == NULL){
			if(raiz->der == NULL){
				hoja = true;
			}
				
		} 
		else {
			intercambio = verificar(raiz->izq, num_original, num_nuevo);
		}
			
	} 
	else if(num_nuevo > raiz->info){
		if(raiz->der == NULL){
			if(raiz->izq == NULL){
				hoja = true;
			}
					
		} 
		else {
			intercambio = verificar(raiz->der, num_original, num_nuevo);
		}
			
	}
	
	if(hoja){
		if(raiz->info == num_original){
			intercambio = true;
			cout<<"encontro una hoja"<<endl;
			cout<<"este nodo es: "<<raiz->info<<endl;
			cout<<"el num origibal es: "<<num_original<<endl;
		}
	}
	
	return intercambio;
	
}

bool Arbol::mayor_de_los_menores(Nodo *nodo, int num){
	/**
	 * 
	 * Esta función busca el nodo mayor dentro de a rama izquierda para
	 * compararlo con el número nuevo que reemplazará la raiz.
	 * 
	 * El nodo que recibe la función es nodo->izq.
	 * 
	 * El número nuevo tine que ser mayor al nodo mayor de la rama izquierda
	 * para que se mantenga el orden.
	 * 
	 * Para eso se verifica que el número nuevo sea mayor al primero de la 
	 * rama izquierda, para así poder bucar en las ramas derechas de nodo->izq.
	 * 
	 * Se crea un nodo auxiliar que se irá moviendo por las ramas derechas 
	 * si es que hay, si no, valadrá lo mismo que el nodo original.
	 * 
	 * Después de que se termine el while, se preguntará si el número 
	 * nuevo es mayor al último nodo (el más a la dercha de los que están
	 * a la izquierda), si es que sí, band, el booleano que verifica si es 
	 * que se puede o no hacer el intercambio, se vuelve verdadero, si no, 
	 * continua falso.
	 * 
	 * */
	
	bool band = false;
	if(num > nodo->info){
		Nodo *aux = nodo;
		while(aux->der != NULL){
			aux = aux->der;
		}
		if(num > aux->info){
			band = true;
		}
	}
	return band;
	
}

bool Arbol::menor_de_los_mayores(Nodo *nodo, int num){
	
	/***
	 * Esta función busca el nodo menor dentro de a rama derecha para
	 * compararlo con el número nuevo que reemplazará la raiz.
	 * 
	 * ocupa de base la función mayor_de_los_menores pero para el 
	 * lado derecho
	 * */
	bool band = false;
	if(num < nodo->info){
		Nodo *aux = nodo;
		while(aux->izq != NULL){
			aux = aux->izq;
		}
		if(num < aux->info){
			band = true;
		}
	}
	return band;
	
}


void Arbol::mostrar_preorden(Nodo *nodo){
	if(nodo != NULL){
		cout<<nodo->info<<" ";
		mostrar_preorden(nodo->izq);
		mostrar_preorden(nodo->der);
	}
}

void Arbol::mostrar_inorden(Nodo *nodo){
	if(nodo != NULL){
		mostrar_preorden(nodo->izq);
		cout<<nodo->info<<" ";
		mostrar_preorden(nodo->der);
	}
	
}

void Arbol::mostrar_posorden(Nodo *nodo){
	if(nodo != NULL){
		mostrar_preorden(nodo->izq);
		mostrar_preorden(nodo->der);
		cout<<nodo->info<<" ";
	}
}
