#ifndef ARBOL_H
#define ARBOL_H


#include <iostream>
using namespace std;

typedef struct _Nodo {
	// estructura básica de un nodo para un arbol
    int info;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;


class Arbol{
	private:
		Nodo *raiz = NULL;
	public:
		Arbol();
		
		void crear_arbol(Nodo *raiz); 
		Nodo *crear_nodo(int num);
		
		void insertar_numero(Nodo *&nodo, int num); 
		
		void eliminar_numero(Nodo *&nodo, int num); 
		
		void modificar_numero(Nodo *raiz, Nodo *&nodo, int num_original, int num_nuevo);
		bool verificar(Nodo *raiz, int num_original, int num_nuevo);
		int buscar_numero_a_inercambiar(Nodo *nodo, int num);
		bool menor_de_los_mayores(Nodo *nodo, int num);
		bool mayor_de_los_menores(Nodo *nodo, int num);
		
		void mostrar_preorden(Nodo *nodo);
		void mostrar_inorden(Nodo *nodo);
		void mostrar_posorden(Nodo *nodo);
		
};
#endif

