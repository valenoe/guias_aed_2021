#include <iostream>
#include <fstream>
using namespace std;

#include "Grafo.h"
#include "Arbol.h"

int main (void){
	
	Arbol *arbol = new Arbol();
	Nodo *raiz = NULL;
	
	arbol->crear_arbol(raiz);
	
	
	int option, numero, condicion, numero_intercambiar;
	Grafo *g = new Grafo();
	do{
		cout<<"Insertar número\t\t\t[1] "<<endl;
		cout<<"Eliminar número\t\t\t[2] "<<endl;
		cout<<"Modificar número\t\t[3] "<<endl;
		cout<<"Mostrar árbol en preorden\t[4] "<<endl;
		cout<<"Mostrar árbol en inorden\t[5] "<<endl;
		cout<<"Mostrar árbol en posorden\t[6] "<<endl;
		cout<<"Crear grafo\t\t\t[7] "<<endl;
		cout<<"Salir\t\t\t\t[0] "<<endl;
		cin>>option;
	
	
		switch(option){
			case 1:
				cout<<"Ingrese un número"<<endl;
				cin>>numero;
				arbol->insertar_numero(raiz, numero);
				break;
			case 2:
				cout<<"Ingrese un número"<<endl;
				cin>>numero;
				arbol->eliminar_numero(raiz, numero);
				break;
			case 3:
				cout<<"Ingrese el numero original: ";
				cin>>numero;
				cout<<"Ingrese el numero por el que lo va a intercambiar: ";
				cin>>numero_intercambiar;
				condicion = arbol->buscar_numero_a_inercambiar(raiz, numero_intercambiar);
	
				if(condicion != 2){
					
					arbol->modificar_numero(raiz, raiz, numero, numero_intercambiar);
				}
				break;
			case 4:
				cout<<"Preorden: ";
				arbol->mostrar_preorden(raiz);
				cout<<endl;
				break;
			case 5:
				cout<<"Inorden: ";
				arbol->mostrar_inorden(raiz);
				cout<<endl;
				break;
			case 6:
				cout<<"posorden: ";
				arbol->mostrar_posorden(raiz);
				cout<<endl;
				break;
			case 7:
				g->crear_grafo(raiz);
				break;
			default: 
				cout<<"Salir"<<endl;
				option =0;
				break;
				
				
		}
		
		
	}while(option != 0);
		
		
		return 0;
		
		
	
}

