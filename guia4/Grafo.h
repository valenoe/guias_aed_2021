#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include <fstream>
using namespace std;
#include "Arbol.h"

class Grafo {
    private:
    
    public:
        Grafo();
        void crear_grafo(Nodo *nodo);
        
        
        void recorrer(Nodo *arbol, ofstream &archivo);
};


#endif
