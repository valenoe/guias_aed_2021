#include <iostream>
#include <fstream>
using namespace std;

#include "Grafo.h"

Grafo::Grafo () {
}
void Grafo::crear_grafo (Nodo *nodo) {
	ofstream archivo;
            
            /* abre archivo */
	archivo.open ("grafo.txt");

    archivo << "digraph G {" << endl;
    archivo << "node [style=filled fillcolor=yellow];" << endl;
                
    recorrer(nodo, archivo);

    archivo << "}" << endl;

            /* cierra archivo */
	archivo.close();
                    
            /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
    system("eog grafo.png &");
}

void Grafo::recorrer(Nodo *arbol, ofstream &archivo) {
	if(arbol != NULL){
		if(arbol->izq != NULL){
			archivo<<"\n"<<arbol->info<<"->"<<arbol->izq->info<<";";
		} else {
			archivo<<"\n"<<'"'<<arbol->info<<"i"<<'"'<<"[shape=point];";
			archivo<<"\n"<<arbol->info<<"->"<<'"'<<arbol->info<<"i"<<'"';
		}
		
		if(arbol->der != NULL){
			archivo<<"\n"<<arbol->info<<"->"<<arbol->der->info<<";";
		} else {
			archivo<<"\n"<<'"'<<arbol->info<<"d"<<'"'<<"[shape=point];";
			archivo<<"\n"<<arbol->info<<"->"<<'"'<<arbol->info<<"d"<<'"';
		}
		
		recorrer(arbol->izq, archivo);
		recorrer(arbol->der, archivo);
		
	}
}
