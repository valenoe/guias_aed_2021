
#include <iostream>
using namespace std;
#include "Dijkstra.h"
#include "Grafo.h"
Dijkstra::Dijkstra(){
}


void Dijkstra::llenar_matriz(int total_nodos, char *nombres){
	matriz = new int*[total_nodos];
	int distancia;
			
			/*inicializar matriz*/
	for(int i = 0; i<total_nodos; i++){
		matriz[i] = new int[total_nodos];
	}
			
			/*Llenar la matriz*/
	for(int i=0; i<total_nodos; i++){
		cout<<endl;
		cout<<"Trabajando en el nodo: "<<nombres[i];
		cout<<endl;
		for(int j=0; j<total_nodos; j++){
			if(i != j){
				cout<<"Ingrese a distancia del nodo "<<nombres[j]<<": ";
				
				cin >> distancia;
				while(distancia<-1 || distancia == 0){
					cout<<endl;
					cout<<  "la distancia  no puede ser menor a -1 ni 0"<<endl;
					cout<<"Ingrese a distancia del nodo "<<nombres[j]<<": ";
					cin >> distancia;
					
				}
				matriz[i][j] = distancia;
			} else {
				/*diagonal de la matriz*/
				 matriz[i][j] = 0;
			}					
		}
	}
}



// copia contenido inicial a D[] desde la matriz M[][].
void Dijkstra::inicializar_vector_D (int *D, int N, int posicion) {
	int col;
  
	for (col=0; col<N; col++) {
		D[col] = matriz[posicion][col];
	}
}

//validar verctor
bool Dijkstra::validar_vertice(char nombre, char*V, int N, int *posicion){
	/*
	 * verifica que el vertice que se pide sea el origen realmente esté en la 
	 * el vector de nombres
	 * */
	int i;
	bool existe_vertice=false;
	for(i=0; i<N; i++){
		if(nombre == V[i]){
			*posicion = i;
			existe_vertice=true;
		}
	}
	return existe_vertice;
			
}

// inicializa con espacios el arreglo de caracteres.
void Dijkstra::inicializar_vector_caracter (char *vector, int N) {
	int col;
  
	for (col=0; col<N; col++) {
		vector[col] = ' ';
		//cout<<col<<" : "<<vector[col];
	}
}


// aplica el algoritmo.
void Dijkstra::aplicar_dijkstra (char *V , char *S , char *VS , int *D, int N) {
	int i;
	int v;
  
	cout<<"Escogja el vertice para el origen"<<endl;
	cout<<V<<endl;
	bool existe_nodo=false;
	int posicion;
	char origen;
	cin>>origen;
	existe_nodo=validar_vertice(origen, V, N, &posicion);
	while(existe_nodo==false){
		cout << "\n";
		cout << "Nodo ingresado no existe. Prueba de nuevo : ";
		cin >> origen;
		existe_nodo=validar_vertice(origen, V, N, &posicion);
	}
  // inicializar vector D[] segun datos de la matriz M[][] 
  // estado inicial.
	inicializar_vector_D(D, N, posicion);

  //
	cout<<"\n-----------------------Estados iniciales ---------------------------------------\n"<<endl;
	
	cout<<endl;
	imprimir_vector_caracter(S, "S", N);
	imprimir_vector_caracter(VS, "VS", N);
	imprimir_vector_entero(D, N);
	cout<<"--------------------------------------------------------------------------------\n\n";

  // agrega primer véctice.
	cout<<"> agrega primer valor V[0] a S[] y actualiza VS[]\n\n";
	agrega_vertice_a_S (S, V[posicion], N);
	imprimir_vector_caracter(S, "S", N);
  //
	actualizar_VS (V, S, VS, N);
	imprimir_vector_caracter(VS, "VS", N);
	imprimir_vector_entero(D, N);

  //
	for (i=1; i<N; i++) {
    // elige un vértice en v de VS[] tal que D[v] sea el mínimo 
		cout<<"\n> elige vertice menor en VS[] según valores en D[]\n";
		cout<<"> lo agrega a S[] y actualiza VS[]\n";
		v = elegir_vertice (VS, D, V, N);

    //
		agrega_vertice_a_S (S, v, N);
		imprimir_vector_caracter(S, "S",N);

    //
		actualizar_VS (V, S, VS, N);
		imprimir_vector_caracter(VS, "VS",N);

    //
		actualizar_pesos(D, VS, V, v, N);
		imprimir_vector_entero(D, N);
	}
}




void Dijkstra::actualizar_pesos (int *D, char *VS, char *V, char v, int N) {
	/**
	 * Esta función se encraga de actualizar los valores del vector D
	 * 
	 * se buscan los indices de v y w, w corresponde a cada una de las letras 
	 * del vector VS, y v a la última letra añadida al vector V
	 * 
	 * Con estos indices se llama a calcular_minimo para actualizar D
	 * por cada uno de los objetos dentro de VS.
	 * */
	int i = 0;
	int indice_w, indice_v;

	cout<<"\n> actualiza pesos en D[]\n";
  
	indice_v = buscar_indice_caracter(V, v, N);
	while (VS[i] != ' ') {
		if (VS[i] != v) {
		indice_w = buscar_indice_caracter(V, VS[i], N);
		D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], matriz[indice_v][indice_w]);
		}
		i++;
	}
}


int Dijkstra::calcular_minimo(int dw, int dv, int mvw) {
	/**
	 * esta función se encraga de caluclar el minimo entre dw y (dv + mvw)
	 * 
	 * -1 significa que no hay camino entre un punto y otro, también se 
	 * representa por un infinito.
	 * */
	int min = 0;
	
  
	if (dw == -1) {
		/* si dw es -1, se pregunta si dv y mvw no son -1, ambos
		 * 
		 * si se cumple esta condicion entonces el minimo va a ser la 
		 * suma de los dos últimos.
		 * 
		 * si no, o sea, si alguno de los dos o mabos son -1, entonces 
		 * el minimo es -1, ya que un n° normal más un infinito da infinito*/
		if (dv != -1 && mvw != -1)
		min = dv + mvw;
		else
		min = -1;

	} else {
		
		/* si dw es un numero entero mayor que 0 se debe perguntar si 
		 * dv y mvw no son -1 para hacer la comparación, el min va a ser el 
		 * menor entre dw y (dv + mvw)
		 * 
		 * si dv y mvw son -1, entonces el minimo es dw*/
		if (dv != -1 && mvw != -1) {
			if (dw <= (dv + mvw))
				min = dw;
			else
				min = (dv + mvw);
		}
		else
			min = dw;
	}
  

	cout<<"dw: "<<dw<<" dv: "<<dv<<" mvw: "<<mvw<<" min: "<<min<<endl;

	return min;
}


void Dijkstra::agrega_vertice_a_S(char *S , char vertice, int N) {
	/*agrega vértice a S[].*/
	int i;
	
  // recorre buscando un espacio vacio.
	for (i=0; i<N; i++) {
		if (S[i] == ' ') {
			S[i] = vertice;
			return;
		}
	}  
}


int Dijkstra::elegir_vertice(char *VS , int *D , char *V , int N) {
	/*elige vértice con menor peso en VS[].
	 * busca su peso en D[].*/
	int i = 0;
	int menor = 0;
	int peso;
	int vertice;

	while (VS[i] != ' ') {
		peso = D[buscar_indice_caracter(V, VS[i], N)];
		
    // descarta valores infinitos (-1) y 0.
		if ((peso != -1) && (peso != 0)) {
			
			if (menor == 0) {
				menor = peso;
				vertice = VS[i];
			} else {
				if (peso < menor) {
					menor = peso;
					vertice = VS[i];
				}
			}
		} 
		if(peso == -1){
			
		/**si peso en algún momento vale -1, el for siguiente va a comprobar que
		 * solo queden valores -1 en VS  para asignar un vertice, para ello pregunta si
		 * el valor de peso de los vertices que queden es -1 y que el resto de VS sean solo espacios 
		 * vacíos, el contador contt va a aumentar cuando ocurra alguna de estas dos condiciones
		 * si contt es igual a N, significa que solo  quedan valores -1, entonces se va a
		 * asignar un vertice */
			int j, contt = 0, peso_aux;
			for(j = 0; j<N; j++){
				peso_aux = D[buscar_indice_caracter(V, VS[j], N)];
				if(peso_aux == -1 || VS[j] == ' '){
					
					contt++;
					
				}
				if(contt== N){
					menor = peso;
					vertice = VS[i];
				}
			}
		
		}

		i++;
	}
  
	cout<<"\nvertice: "<<vertice<<"\n\n";
	return vertice;
}


int Dijkstra::buscar_indice_caracter(char *V , char caracter, int N){
	/*retorna el índice del caracter consultado.*/
	int i;
  
	for (i=0; i<N; i++) {
		if (V[i] == caracter)
			return i;
	}
  
	return i;
}


int Dijkstra::busca_caracter(char c, char *vector, int N) {
	/*busca la aparición de un caracter en un vector de caracteres.*/
	int j;
  
	for (j=0; j<N; j++) {
		if (c == vector[j]) {
			return 1;
		}
	}
  
	return 0;
}


void Dijkstra::actualizar_VS(char *V , char *S , char *VS, int N) {
	/*actualiza VS[] cada ves que se agrega un elemnto a S[].*/
	int j;
	int k = 0;
  
	inicializar_vector_caracter(VS, N);
  
	for (j=0; j<N; j++){
    // por cada caracter de V[] evalua si está en S[],
    // Sino está, lo agrega a VS[].
		if (busca_caracter(V[j], S, N) != 1) {
			VS[k] = V[j];
			k++;
		}
	}
}


void Dijkstra::leer_nodos(char *vector, int N) {
	/**
	 * lee datos de los nodos.
	 * inicializa utilizando código ASCII.*/
	int i;
	int inicio = 97;
  
	for (i=0; i<N; i++) {
		vector[i] = inicio+i;
	}	
}


void Dijkstra::imprimir_vector_caracter(char *vector, string nomVector, int N) {
	/*imprime el contenido de un vector de caracteres.*/
	int i;
  
	for (i=0; i<N; i++) {
		cout<< nomVector<<"["<<i<<"]: "<< vector[i]<<endl;
	}
	cout<<endl;
}


void Dijkstra::imprimir_vector_entero(int *vector, int N) {
	/*Imprime el contenido del vertor D*/
	int i;
  
	for (i=0; i<N; i++) {
    
		cout<<"D["<<i<<"]: "<<vector[i]<<"  ";
	}
	cout<<endl;
}

// imprime el contenido de una matriz bidimensional de enteros.
void Dijkstra::imprimir_matriz( int N){ 
	int i, j;
  
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
			cout<<"matriz["<<i<<","<<j<<"]: "<<matriz[i][j];
			cout<<"\t";
		}
		cout<<endl;
	}
	
}

void Dijkstra::imprimir_grafo(char *vector, int N){
	Grafo *g = new Grafo();
	g->imprimir_grafo(matriz, vector, N);
}
