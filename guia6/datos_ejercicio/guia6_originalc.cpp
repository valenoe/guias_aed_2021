#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#define N 5

void leer_nodos (char vector[N]);
void inicializar_vector_D (int D[N], int M[N][N]);
void inicializar_vector_caracter (char vector[N]);
void aplicar_dijkstra (char V[N], char S[N], char VS[N], int D[N], int M[N][N]);
void actualizar_VS(char V[N], char S[N], char VS[N]);
int buscar_indice_caracter(char V[N], char caracter);
void agrega_vertice_a_S(char S[N], char vertice);
int elegir_vertice(char VS[N], int D[N], char V[N]);
void actualizar_pesos (int D[N], char VS[N], int M[N][N], char V[N], char v); 
int calcular_minimo(int dw, int dv, int mvw);								
void imprimir_vector_caracter(char vector[N], string );
void imprimir_vector_entero(int vector[N]);
void imprimir_matriz(int matriz[N][N]);
void imprimir_grafo(int matriz[N][N], char vector[N]); //grafo

// main
int main(int argc, char **argv){
	
	char V[N], S[N], VS[N];
	int D[N];

  // valores de prueba1.
	int M[N][N] = {{ 0, 4, 11, -1, -1},
                 {-1, 0, -1,  6,  2},
                 {-1 ,3,  0,  6, -1},
                 {-1,-1, -1,  0, -1},
                 {-1,-1,  5,  3,  0}};
                 
	inicializar_vector_caracter(V);
	inicializar_vector_caracter(S);
	inicializar_vector_caracter(VS);
  
  //
	leer_nodos(V);
  
  //
	aplicar_dijkstra (V, S, VS, D, M);

  //  
	imprimir_grafo(M, V);
	
	return 0;
}

// copia contenido inicial a D[] desde la matriz M[][].
void inicializar_vector_D (int D[N], int M[N][N]) {
	int col;
  
	for (col=0; col<N; col++) {
		D[col] = M[0][col];
	}
}

// inicializa con espacios el arreglo de caracteres.
void inicializar_vector_caracter (char vector[N]) {
	int col;
  
	for (col=0; col<N; col++) {
		vector[col] = ' ';
	}
}


// aplica el algoritmo.
void aplicar_dijkstra (char V[N], char S[N], char VS[N], int D[N], int M[N][N]) {
	int i;
	int v;
  
  // inicializar vector D[] segun datos de la matriz M[][] 
  // estado inicial.
	inicializar_vector_D(D, M);

  //
	cout<<"-----------------------Estados iniciales ---------------------------------------\n"<<endl;
	imprimir_matriz(M);
	cout<<endl;
	imprimir_vector_caracter(S, "S");
	imprimir_vector_caracter(VS, "VS");
	imprimir_vector_entero(D);
	cout<<"--------------------------------------------------------------------------------\n\n";

  // agrega primer véctice.
	cout<<"> agrega primer valor V[0] a S[] y actualiza VS[]\n\n";
	agrega_vertice_a_S (S, V[0]);
	imprimir_vector_caracter(S, "S");
  //
	actualizar_VS (V, S, VS);
	imprimir_vector_caracter(VS, "VS");
	imprimir_vector_entero(D);

  //
	for (i=1; i<N; i++) {
    // elige un vértice en v de VS[] tal que D[v] sea el mínimo 
		cout<<"\n> elige vertice menor en VS[] según valores en D[]\n";
		cout<<"> lo agrega a S[] y actualiza VS[]\n";
		v = elegir_vertice (VS, D, V);

    //
		agrega_vertice_a_S (S, v);
		imprimir_vector_caracter(S, "S");

    //
		actualizar_VS (V, S, VS);
		imprimir_vector_caracter(VS, "VS");

    //
		actualizar_pesos(D, VS, M, V, v);
		imprimir_vector_entero(D);
	}
}



//
void actualizar_pesos (int D[N], char VS[N], int M[N][N], char V[N], char v) {
  int i = 0;
  int indice_w, indice_v;

  cout<<"\n> actualiza pesos en D[]\n";
  
  indice_v = buscar_indice_caracter(V, v);
  while (VS[i] != ' ') {
    if (VS[i] != v) {
      indice_w = buscar_indice_caracter(V, VS[i]);
      D[indice_w] = calcular_minimo(D[indice_w], D[indice_v], M[indice_v][indice_w]);
    }
    i++;
  }
}

//
int calcular_minimo(int dw, int dv, int mvw) {
  int min = 0;

  //
  if (dw == -1) {
    if (dv != -1 && mvw != -1)
      min = dv + mvw;
    else
      min = -1;

  } else {
    if (dv != -1 && mvw != -1) {
      if (dw <= (dv + mvw))
        min = dw;
      else
        min = (dv + mvw);
    }
    else
      min = dw;
  }
  
 // cout<<"dw: %d dv: %d mvw: %d min: %d\n", dw, dv, mvw, min);
  cout<<"dw: "<<dw<<" dv: "<<dv<<" mvw: "<<mvw<<" min: "<<min<<endl;

  return min;
}

// agrega vértice a S[].
void agrega_vertice_a_S(char S[N], char vertice) {
  int i;
  
  // recorre buscando un espacio vacio.
  for (i=0; i<N; i++) {
    if (S[i] == ' ') {
      S[i] = vertice;
      return;
    }
  }  
}

// elige vértice con menor peso en VS[].
// busca su peso en D[].
int elegir_vertice(char VS[N], int D[N], char V[N]) {
  int i = 0;
  int menor = 0;
  int peso;
  int vertice;

  while (VS[i] != ' ') {
    peso = D[buscar_indice_caracter(V, VS[i])];
    // descarta valores infinitos (-1) y 0.
    if ((peso != -1) && (peso != 0)) {
      if (i == 0) {
        menor = peso;
        vertice = VS[i];
      } else {
        if (peso < menor) {
          menor = peso;
          vertice = VS[i];
        }
      }
    }

    i++;
  }
  
  cout<<"\nvertice: "<<vertice<<"\n\n";
  return vertice;
}

// retorna el índice del caracter consultado.
int buscar_indice_caracter(char V[N], char caracter) {
  int i;
  
  for (i=0; i<N; i++) {
    if (V[i] == caracter)
      return i;
  }
  
  return i;
}

// busca la aparición de un caracter en un vector de caracteres.
int busca_caracter(char c, char vector[N]) {
  int j;
  
  for (j=0; j<N; j++) {
    if (c == vector[j]) {
      return 1;
    }
  }
  
  return 0;
}

// actualiza VS[] cada ves que se agrega un elemnto a S[].
void actualizar_VS(char V[N], char S[N], char VS[N]) {
  int j;
  int k = 0;
  
  inicializar_vector_caracter(VS);
  
  for (j=0; j<N; j++){
    // por cada caracter de V[] evalua si está en S[],
    // Sino está, lo agrega a VS[].
    if (busca_caracter(V[j], S) != 1) {
      VS[k] = V[j];
      k++;
    }
  }
}

// lee datos de los nodos.
// inicializa utilizando código ASCII.
void leer_nodos (char vector[N]) {
  int i;
  int inicio = 97;
  
  for (i=0; i<N; i++) {
    vector[i] = inicio+i;
  }
}

// imprime el contenido de un vector de caracteres.
void imprimir_vector_caracter(char vector[N], string nomVector) {
  int i;
  
  for (i=0; i<N; i++) {
    cout<< nomVector<<"["<<i<<"]: "<< vector[i]<<endl;
  }
  cout<<endl;
}

//
void imprimir_vector_entero(int vector[N]) {
  int i;
  
  for (i=0; i<N; i++) {
    
    cout<<"D["<<i<<"]: "<<vector[i]<<"  ";
  }
 cout<<endl;
}

// imprime el contenido de una matriz bidimensional de enteros.
void imprimir_matriz(int matriz[N][N]) {
  int i, j;
  
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      cout<<"matriz["<<i<<","<<j<<"]: "<<matriz[i][j];
      cout<<"\t";
    }
    cout<<endl;
  }
}

// genera y muestra apartir de una matriz bidimensional de enteros
// el grafo correspondiente.
void imprimir_grafo(int matriz[N][N], char vector[N]) {
  int i, j;
  ofstream fp;
  
  fp.open("grafo.txt");
  fp <<"digraph G {"<<endl;
  fp <<"graph [rankdir=LR]"<<endl;
  fp <<"node [style=filled fillcolor=violet];"<<endl;
  
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      // evalua la diagonal principal.
      if (i != j) {
        if (matriz[i][j] > 0) {
          //fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"->", vector[j], matriz[i][j]);
          fp << vector[i] <<"->"<< vector[j]<<"[label="<<matriz[i][j]<<"];"<<endl;
        }
      }
    }
  }
  
 // fcout<<fp, "%s\n", "}");
  fp<<"}"<<endl;
  fp.close();

  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
}

