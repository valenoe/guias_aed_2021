#include <iostream>
using namespace std;
#include "Dijkstra.h"
#include "Grafo.h"

int main(int argc, char *argv[]){
	
	if(argc!=2){
		cout << "Comando mal ejecutado, el programa no se podra inciar" << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma : " << endl;
		cout << "Ejemplo: ./Guia6 Numero(Mayor o igual que 2)";
		cout << "\n";
		exit(-1);
	}
	else{
	
		int total_nodos = stoi(argv[1]);
		
		char V[total_nodos], S[total_nodos], VS[total_nodos];
		
		int D[total_nodos];
	
	// se evalúa si el número ingresado sea mayor que dos
		if(total_nodos <= 2){
			cout<<"Debe ingresar un numero mayor que 2"<<endl;
			exit(-1);
			
		} else{
			int opcion;
			Dijkstra *camino_dijkstra = new Dijkstra();
			camino_dijkstra->inicializar_vector_caracter(V, total_nodos);
			camino_dijkstra->inicializar_vector_caracter(S, total_nodos);
			camino_dijkstra->inicializar_vector_caracter(VS, total_nodos);
			
		
			camino_dijkstra-> leer_nodos(V, total_nodos);
		
	
	// se piden los números que van a llenar la matriz 
			camino_dijkstra->llenar_matriz(total_nodos, V);
			cout<<"V "<<V<<endl;
	
			do{
				cout<<endl;
				cout<<"Mostrar matriz\t[1]"<<endl;
				cout<<"Aplicar Dijkstra\t[2]"<<endl;
				cout<<"Mostrar grafo\t[3]"<<endl;
				cout<<"Salir\t\t[0]"<<endl;
				
				
				cin>>opcion;
				switch(opcion){
					case 1:
				
						camino_dijkstra->imprimir_matriz( total_nodos);
						break;
				
					case 2:
					
						camino_dijkstra->aplicar_dijkstra(V, S, VS, D, total_nodos);
						break;
			
					case 3:
						camino_dijkstra->imprimir_grafo(V, total_nodos);
						break;
					
				}
				
			}while(opcion != 0);
			
	
	//imprimir
			
		}
	}
	
	
	

	
	return 0;
	
}

