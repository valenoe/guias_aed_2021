
#include <iostream>
#include <fstream>

#include "Grafo.h"
using namespace std;

Grafo::Grafo () {
}

// genera y muestra apartir de una matriz bidimensional de enteros
// el grafo correspondiente.
void Grafo::imprimir_grafo(int **matriz, char *vector, int N) {
  int i, j;
  ofstream fp;
  
  fp.open("grafo.txt");
  fp <<"digraph G {"<<endl;
  fp <<"graph [rankdir=LR]"<<endl;
  fp <<"node [style=filled fillcolor=violet];"<<endl;
  
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      // evalua la diagonal principal.
      if (i != j) {
        if (matriz[i][j] > 0) {
          fp << vector[i] <<"->"<< vector[j]<<"[label="<<matriz[i][j]<<"];"<<endl;
        }
        
      }
    }
  }
  
 
  fp<<"}"<<endl;
  fp.close();

  system("dot -Tpng -ografo.png grafo.txt");
  system("eog grafo.png &");
  
}
