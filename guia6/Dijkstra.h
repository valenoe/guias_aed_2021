#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <iostream>
using namespace std;
#include "Grafo.h"


class Dijkstra{
	private: 
		int **matriz;
	public:
		Dijkstra();
		
		
		
		void llenar_matriz(int total_nodos, char *nombres);
		
	
		
		void leer_nodos (char *vector, int N);
		void inicializar_vector_D (int *D, int N, int posicion);
		void inicializar_vector_caracter (char *vector, int N);
		void aplicar_dijkstra (char *V , char *S , char *VS , int *D, int N);
		void actualizar_VS(char *V , char *S , char *VS, int N);
		int buscar_indice_caracter(char *V , char caracter, int N);
		void agrega_vertice_a_S(char *S , char vertice, int N);
		int elegir_vertice(char *VS , int *D , char *V , int N);
		void actualizar_pesos (int *D, char *VS, char *V, char v, int N); 
		int calcular_minimo(int dw, int dv, int mvw);								
		void imprimir_vector_caracter(char *vector, string, int N);
		void imprimir_vector_entero(int *vector, int N);
		void imprimir_matriz(int N);
		int busca_caracter(char c, char *vector, int N);
		void imprimir_grafo(char *vector, int N);
		
		bool validar_vertice(char nombre, char*V, int N, int *posicion);
			 
			 
};



#endif
