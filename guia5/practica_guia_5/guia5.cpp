#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;
/*
#define TRUE 1
#define FALSE 0
* */

/* estructura del nodo */
typedef struct _Nodo {
	struct _Nodo *izq;
	struct _Nodo *der;
	int info;
	int FE;
} Nodo;

/* prototipos */
/*
void InsercionBalanceado(Nodo *&nodocabeza, bool BO, int infor);
void Busqueda(Nodo *nodo, int infor);
void Restructura1(Nodo *&nodocabeza, bool BO);
void Restructura2(Nodo *&nodocabeza, bool BO);
void Borra(Nodo *&aux1, Nodo *&otro1, bool BO);
void EliminacionBalanceado(Nodo *&nodocabeza, bool BO, int infor);
int Menu();
void GenerarGrafo(Nodo *p);
void PreOrden(Nodo *a, ofstream &fp);
*/



/* 
 * Muestra un menu de opciones.
 */
int Menu() {
	int Op;
	
	do {
		cout<<"\n--------------------\n"<<endl;
		cout<<"1) Insertar\n"<<endl;
		cout<<"2) Buscar\n"<<endl;
		cout<<"3) Eliminacion\n"<<endl;
		cout<<"4) Grafo\n"<<endl;
		cout<<"0) Salir\n\n"<<endl;
		cout<<"Opción: "<<endl;
		cin>>Op;
	} while (Op<0 || Op>7);
  
	return Op;
}

/* */
int InsercionBalanceado(Nodo *&nodocabeza, int BO, int infor) {
	cout<<"BO inicio: "<<BO<<endl;
	Nodo *nodo = NULL;
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL; 
  
    cout<<"antes"<<endl;
	nodo = nodocabeza;
	cout<<"despues"<<endl;
  
	if (nodo != NULL) {
		cout<<"dentro if 1"<<endl;
    
		if (infor < nodo->info) {
			BO = InsercionBalanceado(nodo->izq, BO, infor);
			
			cout<<"Después de volver de nulo: "<<BO<<endl;
			if(BO == 1) {
				
				switch (nodo->FE) {
					case 1: 
						nodo->FE = 0;
						BO = 0;
						break;
          
					case 0: 
						nodo->FE = -1;
						break;
     
					case -1: 
					/* reestructuración del árbol */
						nodo1 = nodo->izq;
            
				/* Rotacion II */
						if (nodo1->FE <= 0) { 
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->FE = 0;
							nodo = nodo1;
            
						} else { 
				/* Rotacion ID */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
         
							if (nodo2->FE == -1){
								nodo->FE = 1;
							}else{
								nodo->FE =0;
							}
					
							if (nodo2->FE == 1){
								nodo1->FE = -1;
							}else {
								nodo1->FE = 0;
							}
					
							nodo = nodo2;
						}
            
						nodo->FE = 0;
						BO = 0;
						break;
				}
			} 
      
		} else {	
      
			if (infor > nodo->info) {
				BO = InsercionBalanceado(nodo->der, BO, infor);
        
				if (BO == 1) {
			
					switch (nodo->FE) {
            
						case -1: 
							nodo->FE = 0;
							BO = 0;
							break;
           
						case 0: 
							nodo->FE = 1;
							break;
     
						case 1: 
			 			/* reestructuración del árbol */
							nodo1 = nodo->der;
              
							if (nodo1->FE >= 0) { 
								/* Rotacion DD */
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->FE = 0;
								nodo = nodo1;
                
							} else { 
									/* Rotacion DI */
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;
                
								if (nodo2->FE == 1){
									nodo->FE = -1;
								}else{
									nodo->FE = 0;
								}
       
								if (nodo2->FE == -1){
									nodo1->FE = 1;
								}else{
									nodo1->FE = 0;
								}				
								nodo = nodo2;
							}
              
							nodo->FE = 0;
							BO = 0;
							break;
					}	
				}
			} else {
				cout<<"El nodo ya se encuentra en el árbol"<<endl;
			}
		}
	} else {
		cout<<"dentro else 1"<<endl;
    
		nodo = new Nodo;
		cout<<"1"<<endl;
		nodo->izq = NULL;
		cout<<"2"<<endl;
		nodo->der = NULL;
		cout<<"3"<<endl;
		nodo->info = infor;
		cout<<"4"<<endl;
		nodo->FE = 0;
		cout<<"5"<<endl;
		BO = 1;
		cout<<"BO si está nulo: "<<BO<<endl;
	}
  
	nodocabeza = nodo;
	return BO;
}

/* */
void Busqueda(Nodo *nodo, int infor) {
	if (nodo != NULL) {
		if (infor < nodo->info) {
			Busqueda(nodo->izq,infor);
		} else {
			if (infor > nodo->info) {
				Busqueda(nodo->der,infor);
			} else {
				cout<<"El nodo SI se encuentra en el árbol\n";
			}
		}
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n";
	}
}

/* */
void Restructura1(Nodo *&nodocabeza, int BO) {
	Nodo *nodo, *nodo1, *nodo2; 
	nodo = nodocabeza;
  
	if (BO == 1) {
		switch (nodo->FE) {
			case -1: 
				nodo->FE = 0;
				break;
      
			case 0: 
				nodo->FE = 1;
				BO = 0;
				break;
   
			case 1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->der;
      
				if (nodo1->FE >= 0) { 
				/* rotacion DD */
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;
        
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = 1;
							nodo1->FE = -1;
							BO = 0;
							break;
						case 1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = 0;
							break;           
					}
					nodo = nodo1;
				} else { 
					/* Rotacion DI */
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
       
					if (nodo2->FE == 1){
						nodo->FE = -1;
					}else{
						nodo->FE = 0;
					}
						
					if (nodo2->FE == -1){
						nodo1->FE = 1;
					}else{
						nodo1->FE = 0;
					}
					
					nodo = nodo2;
					nodo2->FE = 0;       
				} 
				break;   
		}		
	}
	nodocabeza=nodo;
}

/* */
void Restructura2(Nodo *&nodocabeza, int BO) {
	Nodo *nodo, *nodo1, *nodo2; 	
	nodo = nodocabeza;
  
	if (BO == 1) {
		switch (nodo->FE) {
			case 1: 
				nodo->FE = 0;
				break;
			case 0: 
				nodo->FE = -1;
				BO = 0;
				break;
			case -1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->izq;
				if (nodo1->FE<=0) { 
					/* rotacion II */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = -1;
							nodo1->FE = 1;
							BO = 0;
							break;
						case -1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = 0;
							break;
					}
					nodo = nodo1;
				} else { 
					/* Rotacion ID */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;
       
						if (nodo2->FE == -1){
							nodo->FE = 1;
						}else{
							nodo->FE = 0;
						}
        
						if (nodo2->FE == 1){
							nodo1->FE = -1;
						}else{
							nodo1->FE = 0;
						}
       
						nodo = nodo2;
						nodo2->FE = 0;       
				}      
				break;   
		}
	}
	nodocabeza = nodo;
}


/* */
void Borra(Nodo *&aux1, Nodo *&otro1, int BO) {
	Nodo *aux, *otro; 
	aux=aux1;
	otro=otro1;
  
	if (aux->der != NULL) {
		Borra(aux->der,otro,BO); 
		Restructura2(aux,BO);
	} else {
		otro->info = aux->info;
		aux = aux->izq;
		BO = 1;
	}
	aux1=aux;
	otro1=otro;
}

/* */
void EliminacionBalanceado(Nodo *&nodocabeza, int BO, int infor) {
	Nodo *nodo, *otro; 
	
	nodo = nodocabeza;
  
	if (nodo != NULL) {
		if (infor < nodo->info) {
			EliminacionBalanceado(nodo->izq, BO, infor);
			Restructura1(nodo,BO);
		} else {
			if (infor > nodo->info) {
				EliminacionBalanceado(nodo->der, BO, infor);
				Restructura2(nodo,BO); 
			} else {
				otro = nodo;
				if (otro->der == NULL) {
					nodo = otro->izq;
					BO = 1;     
				} else {
					if (otro->izq==NULL) {
						nodo=otro->der;
						BO = 1;  
					} else {
						Borra(otro->izq,otro,BO);
						Restructura1(nodo,BO);
						free(otro);
					}
				}
			}
		} 
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n"<<endl;
	}
	nodocabeza=nodo;
}

/* */
void PreOrden(Nodo *a, ofstream &fp) {
  
  
	if (a != NULL) {
		if (a->izq != NULL) {
			fp << a->info <<"->"<<a->izq->info<< " [label="<<a->izq->FE<<"];"<<endl; 
		} else{
      
			fp << '"'<< a->info << "i" <<'"'<<" [shape=point];"<<endl;
			fp<<a->info<<"->"<<'"'<< a->info << "i" <<'"'<<";"<<endl; 
		}
    
		if (a->der != NULL) {
			fp << a->info <<"->"<<a->der->info<< " [label="<<a->der->FE<<"];"<<endl;  
		} else{
     
			fp << '"'<< a->info << "d" <<'"'  <<" [shape=point];"<<endl;
			fp<< a->info<<"->"<<'"'<< a->info << "d" <<'"'<<";"<<endl; 
		}

		PreOrden(a->izq,fp);
		PreOrden(a->der,fp); 
	}
}
/* */
void GenerarGrafo(Nodo *ArbolInt) {
	ofstream fp;
	
	fp.open("grafo.txt");
	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=yellow];" << endl;
  
	fp << "nullraiz [shape=point];" << endl; 
	fp << "nullraiz->"<<ArbolInt->info <<"[label="<<ArbolInt->FE<<"];" << endl; 
  
	PreOrden(ArbolInt, fp);
  
	fp << "}" << endl;
	fp.close();
  
	system("dot -Tpng -ografo.png grafo.txt &");
	system("eog grafo.png &");
}



int main(int argc, char **argv) {
	int opcion;
	int elemento;
	Nodo *raiz = NULL;
  
	system("clear");
	opcion = Menu();
	int inicio;
	/*inicio = true;
	cout<<inicio<<endl;
	inicio = false;
	cout<<inicio<<endl;
	* */
  
    while (opcion) {
    
		if(opcion == 1){
			
				cout<<"Ingresar elemento: "<<endl;
				cin>>elemento;
				inicio=0;
				InsercionBalanceado(raiz, inicio, elemento);
				GenerarGrafo(raiz);
			
		} else if(opcion == 2){
			
				cout<<"Buscar elemento: "<<endl;
				cin>>elemento;
				Busqueda(raiz, elemento);
			
		} else if(opcion == 3){
			
				cout<<"Eliminar elemento: "<<endl;
				cin>>elemento;
				inicio=0;
				EliminacionBalanceado(raiz, inicio, elemento);
				GenerarGrafo(raiz);
				
		}else if(opcion == 4){
				GenerarGrafo(raiz);
				
		} else {
				exit(0);
		}
    
		opcion = Menu();
	}
  
	return 0;
}
		
