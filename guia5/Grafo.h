#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include <fstream>

#include "A_balanceado.h"
using namespace std;



class Grafo{
	
	private:
		
	public:
		Grafo();
		void GenerarGrafo(Nodo *p);
		void PreOrden(Nodo *a, ofstream &fp);
		
		
};
#endif
