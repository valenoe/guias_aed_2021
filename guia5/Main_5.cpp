#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "A_balanceado.h"
#include "Grafo.h"

int main(int argc, char **argv) {
	int opcion;
	int elemento;
	A_balanceado *arbol = new A_balanceado();
	Nodo *raiz = NULL;
	Grafo *g = new Grafo();
  
	system("clear");
	opcion = arbol->Menu();
	bool inicio;
  
    while (opcion) {
    
		switch(opcion) {
			case 1:
				cout<<"Ingresar elemento: "<<endl;
				cin>>elemento;
				inicio=false;
				arbol->InsercionBalanceado(raiz, inicio, elemento);
				g->GenerarGrafo(raiz);
				break;
			case 2:
				cout<<"Buscar elemento: "<<endl;
				cin>>elemento;
				arbol->Busqueda(raiz, elemento);
				break;
			case 3:
				cout<<"Eliminar elemento: "<<endl;
				cin>>elemento;
				inicio=false;
				arbol->EliminacionBalanceado(raiz, inicio, elemento);
				g->GenerarGrafo(raiz);
				break;
			case 4:
				g->GenerarGrafo(raiz);
				break;
      
			case 0: 
				exit(0);
		}
    
		opcion = arbol->Menu();
	}
  
	return 0;
}
