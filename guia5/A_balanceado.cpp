#include <iostream>
#include <fstream>
#include <stdlib.h>
using namespace std;

#include "A_balanceado.h"
A_balanceado::A_balanceado(){
}

int A_balanceado::Menu() {
	int Op;
	
	do {
		cout<<"\n--------------------\n"<<endl;
		cout<<"1) Insertar\n"<<endl;
		cout<<"2) Buscar\n"<<endl;
		cout<<"3) Eliminacion\n"<<endl;
		cout<<"4) Grafo\n"<<endl;
		cout<<"0) Salir\n\n"<<endl;
		cout<<"Opción: "<<endl;
		cin>>Op;
	} while (Op<0 || Op>7);
  
	return Op;
}

/* */
void A_balanceado::InsercionBalanceado(Nodo *&nodocabeza, bool BO, int infor) {
	
	Nodo *nodo = NULL;
	Nodo *nodo1 = NULL;
	Nodo *nodo2 = NULL; 
  
	nodo = nodocabeza;
  
	if (nodo != NULL) {
    
		if (infor < nodo->info) {
			InsercionBalanceado(nodo->izq, BO, infor);
      
			if(BO) {
        
				switch (nodo->FE) {
					case 1: 
						nodo->FE = 0;
						BO = false;
						break;
          
					case 0: 
						nodo->FE = -1;
						break;
     
					case -1: 
					/* reestructuración del árbol */
						nodo1 = nodo->izq;
            
				/* Rotacion II */
						if (nodo1->FE <= 0) { 
							nodo->izq = nodo1->der;
							nodo1->der = nodo;
							nodo->FE = 0;
							nodo = nodo1;
            
						} else { 
				/* Rotacion ID */
							nodo2 = nodo1->der;
							nodo->izq = nodo2->der;
							nodo2->der = nodo;
							nodo1->der = nodo2->izq;
							nodo2->izq = nodo1;
         
							if (nodo2->FE == -1){
								nodo->FE = 1;
							}else{
								nodo->FE =0;
							}
					
							if (nodo2->FE == 1){
								nodo1->FE = -1;
							}else {
								nodo1->FE = 0;
							}
					
							nodo = nodo2;
						}
            
						nodo->FE = 0;
						BO = false;
						break;
				}
			} 
      
		} else {	
      
			if (infor > nodo->info) {
				InsercionBalanceado(nodo->der, BO, infor);
        
				if (BO) {
			
					switch (nodo->FE) {
            
						case -1: 
							nodo->FE = 0;
							BO = false;
							break;
           
						case 0: 
							nodo->FE = 1;
							break;
     
						case 1: 
			 			/* reestructuración del árbol */
							nodo1 = nodo->der;
              
							if (nodo1->FE >= 0) { 
								/* Rotacion DD */
								nodo->der = nodo1->izq;
								nodo1->izq = nodo;
								nodo->FE = 0;
								nodo = nodo1;
                
							} else { 
									/* Rotacion DI */
								nodo2 = nodo1->izq;
								nodo->der = nodo2->izq;
								nodo2->izq = nodo;
								nodo1->izq = nodo2->der;
								nodo2->der = nodo1;
                
								if (nodo2->FE == 1){
									nodo->FE = -1;
								}else{
									nodo->FE = 0;
								}
       
								if (nodo2->FE == -1){
									nodo1->FE = 1;
								}else{
									nodo1->FE = 0;
								}				
								nodo = nodo2;
							}
              
							nodo->FE = 0;
							BO = false;
							break;
					}	
				}
			} else {
				cout<<"El nodo ya se encuentra en el árbol"<<endl;
			}
		}
	} else {
    
		nodo = new _Nodo;
		nodo->izq = NULL;
		nodo->der = NULL;
		nodo->info = infor;
		nodo->FE = 0;
		BO = true;
	}
  
	nodocabeza = nodo;
}

/* */
void A_balanceado::Busqueda(Nodo *nodo, int infor) {
	if (nodo != NULL) {
		if (infor < nodo->info) {
			Busqueda(nodo->izq,infor);
		} else {
			if (infor > nodo->info) {
				Busqueda(nodo->der,infor);
			} else {
				cout<<"El nodo SI se encuentra en el árbol\n";
			}
		}
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n";
	}
}

/* */
void A_balanceado::Restructura1(Nodo *&nodocabeza, bool BO) {
	Nodo *nodo, *nodo1, *nodo2; 
	nodo = nodocabeza;
  
	if (BO) {
		switch (nodo->FE) {
			case -1: 
				nodo->FE = 0;
				break;
      
			case 0: 
				nodo->FE = 1;
				BO = false;
				break;
   
			case 1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->der;
      
				if (nodo1->FE >= 0) { 
				/* rotacion DD */
					nodo->der = nodo1->izq;
					nodo1->izq = nodo;
        
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = 1;
							nodo1->FE = -1;
							BO = false;
							break;
						case 1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = false;
							break;           
					}
					nodo = nodo1;
				} else { 
					/* Rotacion DI */
					nodo2 = nodo1->izq;
					nodo->der = nodo2->izq;
					nodo2->izq = nodo;
					nodo1->izq = nodo2->der;
					nodo2->der = nodo1;
       
					if (nodo2->FE == 1){
						nodo->FE = -1;
					}else{
						nodo->FE = 0;
					}
						
					if (nodo2->FE == -1){
						nodo1->FE = 1;
					}else{
						nodo1->FE = 0;
					}
					
					nodo = nodo2;
					nodo2->FE = 0;       
				} 
				break;   
		}		
	}
	nodocabeza=nodo;
}

/* */
void A_balanceado::Restructura2(Nodo *&nodocabeza, bool BO) {
	Nodo *nodo, *nodo1, *nodo2; 	
	nodo = nodocabeza;
  
	if (BO) {
		switch (nodo->FE) {
			case 1: 
				nodo->FE = 0;
				break;
			case 0: 
				nodo->FE = -1;
				BO = false;
				break;
			case -1: 
				/* reestructuracion del árbol */
				nodo1 = nodo->izq;
				if (nodo1->FE<=0) { 
					/* rotacion II */
					nodo->izq = nodo1->der;
					nodo1->der = nodo;
					switch (nodo1->FE) {
						case 0: 
							nodo->FE = -1;
							nodo1->FE = 1;
							BO = false;
							break;
						case -1: 
							nodo->FE = 0;
							nodo1->FE = 0;
							BO = false;
							break;
					}
					nodo = nodo1;
				} else { 
					/* Rotacion ID */
					nodo2 = nodo1->der;
					nodo->izq = nodo2->der;
					nodo2->der = nodo;
					nodo1->der = nodo2->izq;
					nodo2->izq = nodo1;
       
						if (nodo2->FE == -1){
							nodo->FE = 1;
						}else{
							nodo->FE = 0;
						}
        
						if (nodo2->FE == 1){
							nodo1->FE = -1;
						}else{
							nodo1->FE = 0;
						}
       
						nodo = nodo2;
						nodo2->FE = 0;       
				}      
				break;   
		}
	}
	nodocabeza = nodo;
}


/* */
void A_balanceado::Borra(Nodo *&aux1, Nodo *&otro1, bool BO) {
	Nodo *aux, *otro; 
	aux=aux1;
	otro=otro1;
  
	if (aux->der != NULL) {
		Borra(aux->der,otro,BO); 
		Restructura2(aux,BO);
	} else {
		otro->info = aux->info;
		aux = aux->izq;
		BO = true;
	}
	aux1=aux;
	otro1=otro;
}

/* */
void A_balanceado::EliminacionBalanceado(Nodo *&nodocabeza, bool BO, int infor) {
	Nodo *nodo, *otro; 
	
	nodo = nodocabeza;
  
	if (nodo != NULL) {
		if (infor < nodo->info) {
			EliminacionBalanceado(nodo->izq, BO, infor);
			Restructura1(nodo,BO);
		} else {
			if (infor > nodo->info) {
				EliminacionBalanceado(nodo->der, BO, infor);
				Restructura2(nodo,BO); 
			} else {
				otro = nodo;
				if (otro->der == NULL) {
					nodo = otro->izq;
					BO = true;     
				} else {
					if (otro->izq==NULL) {
						nodo=otro->der;
						BO=true;     
					} else {
						Borra(otro->izq,otro,BO);
						Restructura1(nodo,BO);
						free(otro);
					}
				}
			}
		} 
	} else {
		cout<<"El nodo NO se encuentra en el árbol\n"<<endl;
	}
	nodocabeza=nodo;
}


	

