#ifndef A_BALANCEADO_H
#define A_BALANCEADO_H

#include <iostream>
using namespace std;

/* estructura del nodo */
typedef struct _Nodo {
	struct _Nodo *izq;
	struct _Nodo *der;
	int info;
	int FE;
} Nodo;

class A_balanceado{
	
	private:
		
	public:
		A_balanceado();
		void InsercionBalanceado(Nodo *&nodocabeza, bool BO, int infor);
		void Busqueda(Nodo *nodo, int infor);
		void Restructura1(Nodo *&nodocabeza, bool BO);
		void Restructura2(Nodo *&nodocabeza, bool BO);
		void Borra(Nodo *&aux1, Nodo *&otro1, bool BO);
		void EliminacionBalanceado(Nodo *&nodocabeza, bool BO, int infor);
		int Menu();
		
};
#endif
