#include <iostream>
#include <cstring>
using namespace std;

#include "Prim.h"
Prim::Prim(){}
		

void Prim::inicializar_L(int total_nodos){
	/**L va inicializarse como una matriz con
	 * valores falsos, estos solo van a cambiar
	 * cuando se encuentren los valores minimos entre los nodos*/
	L = new int*[total_nodos];
	
			
			
	for(int i = 0; i<total_nodos; i++){
		L[i] = new int[total_nodos];
		memset(L[i], false, total_nodos); // convierte a falso
	}

	
	
	
}
void Prim::nombres_matriz(int total_nodos, char *nombres){
	/*función que pide los nombes para identificar mejor a los nodos, 
	 * el arreglo que se crea es de tipo char*/
	char Nombre;
		for(int i=0;i<total_nodos;i++){
			cout << "\n";
			cout << "Ingrese un nombre para el nodo " << i+1 << ": ";
			cin >> Nombre;
			nombres[i]=Nombre;
		}
	
}
void Prim::llenar_matriz(int total_nodos, char *nombres){
	matriz = new int*[total_nodos];
	int distancia;
			
			/*inicializar matriz*/
	for(int i = 0; i<total_nodos; i++){
		matriz[i] = new int[total_nodos];
	}
	inicializar_L(total_nodos);
			
			/*Llenar la matriz*/
	for(int i=0; i<total_nodos; i++){
		cout<<endl;
		if(i <total_nodos-1){
			cout<<"Trabajando en el nodo: "<<nombres[i];
		}
		cout<<endl;
		for(int j=0; j<total_nodos; j++){
			if(i != j){
				/*solo se van a pedir los valores cuando j sea mayora i para 
				 * que los valores de la matris sean iguales entre los mimso nodos.
				 * ej: a -- b == b -- a*/
				if(j > i){
					
					cout<<"Ingrese la distancia del nodo "<<nombres[j]<<": ";
				
					cin >> distancia;
					while(distancia < 0 ){
						cout<<endl;
						cout<<  "la distancia  no puede ser menor a 0"<<endl;
						cout<<"Ingrese la distancia del nodo "<<nombres[j]<<": ";
						cin >> distancia;
					
					}
					/*Se guarda el valor en ambas posiciones*/
					matriz[i][j] = distancia;
					matriz[j][i] = distancia;
				}
			} else {
				/*diagonal de la matriz*/
				 matriz[i][j] = 0;
			}					
		}
	}
}

void Prim::aplicar_prim(int total_nodos, char *nombres){
	int arista =0; // numero de aristas

  /*S va a ser un arreglo de nodos falso originalmente,
   * que se va a ir convirtiendo en verdadero cada vez que 
   * encuantre la arista de menos peso*/
	int S[total_nodos];

  // establecer S falso inicialmente
	memset(S, false, sizeof(S));


  // choose 0th vertex and make it true
	S[0] = true;
  

	int x;  //  row number
	int y;  //  col number

  // print for edge and weight
	cout << "L = {}"
		<< " : "
		<< "Peso";
	cout << endl;
	while (arista < total_nodos - 1) {
    
	/*Busca la arista de menor peso entre cada nodo con 3 condiciones
	 * 
	 * 						A ------------------------- X
	 * 			nodo original ya existente         nodo nuevo
	 * 
	 * 1) S[i] exista -> solo se pueden conectar nodos si es que 
	 * 		la posisción de A en S, S[i], es real
	 * 2) Para asegurare que el nuevo nodo más cercano realmente es nuevo
	 * o diferente, su posición en S, S[j], debe er falsa
	 * 3) matriz[i][j] existe, o sea, no se aditen 0, ya que 0 significa 
	 * o que es el mismo, o que no hay conexión 
	 * 
	 * Esta función va a recorrer toda la matriz buscandoconexiones entre 
	 * losnodos fatantes, por ende A puede cualquier nodo, no necesariamente
	 * el último agregado 
	 * 
	 * */
    int min = 99999999;
    x = 0;
    y = 0;

	for (int i = 0; i < total_nodos; i++) {
		if (S[i]) {
			for (int j = 0; j < total_nodos; j++) {
				if (!S[j] && matriz[i][j]) { 
					if (min >= matriz[i][j]) {
						min = matriz[i][j];
						x = i;
						y = j;
              
					}
				}
			}
		}
	}
	/*Se imprimen los valores de costo minimo*/
    cout << nombres[x] << " - " << nombres[y] << " :  " << matriz[x][y];
    /*y se cambia a true la posicion encontrada en la matriz original, pero en L
     * para así poder hacer el grafo*/
    L[x][y] = true;
    cout << endl;
    
    S[y] = true;
    arista++;
    
  }
  
 }


void Prim::mostrar_matriz(int total_nodos, char *nombres){
	/*Esta función se encraga de mostra la matriz con os nombres
	 * de cada lista*/
	cout<<endl;
	cout<<"    ";
	for(int i=0; i<total_nodos; i++){
		cout<<nombres[i]<<"   ";
	}
	cout<<endl;
	cout<<"___";
	for(int i=0; i<total_nodos; i++){
		cout<<"____";
	}
	cout<<endl;
	for(int i=0;i<total_nodos;i++){
		cout<<nombres[i]<<"|  ";
			for(int j=0;j<total_nodos;j++){
				cout << matriz[i][j] << "   ";
			}
			cout << "\n";
		}
	
}


void Prim::grafo_original(char *nombres, int total_nodos){
	// mustra el  grafo original
	Grafo *g = new Grafo();
	g->imprimir_grafo_original(matriz, nombres, total_nodos);
}

void Prim::grafo_costo_minimo(char *nombres, int total_nodos){
	// mustra el grafo de costo mínimo
	Grafo *k = new Grafo();
	k->imprimir_grafo_costo_minimo(matriz, nombres, total_nodos, L);
}
