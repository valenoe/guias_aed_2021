// Prim's Algorithm in C++

#include <cstring>
#include <iostream>
using namespace std;

#include "Prim.h"



int main(int argc, char *argv[]){

  
	if(argc!=2){
		cout << "Comando mal ejecutado, el programa no se podra inciar" << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma : " << endl;
		cout << "Ejemplo: ./prim Numero(Mayor o igual que 2)";
		cout << "\n";
		exit(-1);
	}
	else{
	
		int total_nodos = stoi(argv[1]);
		
		char nombres[total_nodos];
		
	
	// se evalúa si el número ingresado sea mayor que dos
		if(total_nodos <= 2){
			cout<<"Debe ingresar un numero mayor que 2"<<endl;
			exit(-1);
			
		} else{
			
			cout<<" \nB I E N V E N I D O  A L  A L G O R I T M O  D E  P R I M \n\n";
			int opcion;
			Prim *prim = new Prim();
			prim->nombres_matriz(total_nodos, nombres);
			prim->llenar_matriz(total_nodos, nombres);
		
			do{
				cout<<endl;
				cout<<"Mostrar matriz\t\t\t[1]"<<endl;
				cout<<"Aplicar Algoritmo de Prim\t[2]"<<endl;
				cout<<"Mostrar grafo original\t\t[3]"<<endl;
				cout<<"Mostrar grafo de costo minimo\t[4]"<<endl;
				cout<<"Salir\t\t\t\t[0]"<<endl;
				
				
				cin>>opcion;
				switch(opcion){
					case 1:
				
						prim->mostrar_matriz(total_nodos, nombres);
						break;
				
					case 2:
					
						prim->aplicar_prim(total_nodos, nombres);
						break;
			
					case 3:
						prim->grafo_original(nombres, total_nodos);
						break;
						
					case 4:
						prim->grafo_costo_minimo(nombres, total_nodos);
						break;
					
				}
				
			}while(opcion != 0);
		}
			
	}
  return 0;
}

