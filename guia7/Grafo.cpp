
#include <iostream>
#include <fstream>

#include "Grafo.h"
using namespace std;

Grafo::Grafo () {
}

// genera y muestra a partir de una matriz bidimensional de enteros
// el grafo correspondiente.
void Grafo::imprimir_grafo_original(int **matriz, char *vector, int N) {
  int i, j;
  ofstream fp;
  
  fp.open("grafo1.txt");
  fp <<"graph G {"<<endl;
  fp <<"graph [rankdir=LR]"<<endl;
  fp <<"node [style=filled fillcolor=violet];"<<endl;
  fp <<"\n"<<endl;
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      // evalua la diagonal principal.
      if (i != j) {
		  /*j debe ser mayor a i o i debe ser menor a j. Así no se impirmen
		   * dos veces los caminos*/
        if (matriz[i][j] > 0 && j>i) {
          fp << vector[i] <<" -- "<< vector[j]<<" [label="<<matriz[i][j]<<"];"<<endl;
        }
        
      }
    }
  }
  
 
  fp<<"}"<<endl;
  fp.close();

  system("dot -Tpng -ografo1.png grafo1.txt");
  system("eog grafo1.png &");
  
}


void Grafo::imprimir_grafo_costo_minimo(int **matriz, char *vector, int N, int **L){
	int i, j;
  ofstream fp;
  
  fp.open("grafo2.txt");
  fp <<"graph G {"<<endl;
  fp <<"graph [rankdir=LR]"<<endl;
  fp <<"node [style=filled fillcolor=lightblue];"<<endl;
  fp <<"\n"<<endl;
  
  for (i=0; i<N; i++) {
    for (j=0; j<N; j++) {
      // evalua que esos sean los costos minimos.
      // L es uma matrz que guarda en las posiciones de los costos
      // minimos un 1 (true) y en el resto 0 (false)
      if (L[i][j]) {
       
          fp << vector[i] <<" -- "<< vector[j]<<" [label="<<matriz[i][j]<<"];"<<endl;
        
        
      }
    }
  }
  
 
  fp<<"}"<<endl;
  fp.close();

  system("dot -Tpng -ografo2.png grafo2.txt");
  system("eog grafo2.png &");
	
}


