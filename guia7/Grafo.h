#ifndef GRAFO_H
#define GRAFO_H

#include <iostream>
#include <fstream>
using namespace std;



class Grafo {
    private:
    
    public:
        Grafo();
		void imprimir_grafo_original(int **matriz, char *vector, int N);
		void imprimir_grafo_costo_minimo(int **matriz, char *vector, int N, int **L);
};


#endif
