#ifndef PRIM_H
#define PRIM_H

#include <iostream>
using namespace std;
#include "Grafo.h"

class Prim{
	private: 
		int **matriz;
		int **L;
	public:
		Prim();
		void nombres_matriz(int total_nodos, char *nombres);
		void llenar_matriz(int total_nodos, char *nombres);
		void aplicar_prim(int total_nodos, char *nombres);
		void mostrar_matriz(int total_nodos, char *nombres);
		void inicializar_L(int total_nodos);
		void grafo_original(char *nombres, int total_nodos);
		void grafo_costo_minimo(char *nombres, int total_nodos);

};



#endif
